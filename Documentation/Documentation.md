<img align="right" src="images/telerik_logo.PNG" alt="Telerik Academy Logo" />


<img align="center" src="images/social_network.PNG" alt="Social Network Logo" />

<div align="center">

# SOCIABLE WEAVER DOCUMENTATION
</div>

Available [active Project documents](https://drive.google.com/open?id=1ENOqv4Kwru1KaLXjDdNjXXWLXK0GWbA-) can be located via the links below:

* Project Specifications: [DEV](https://drive.google.com/open?id=1zr30YVZQFtjWFrJuNinfw-rfI6nEpuTu)
* Project Specifications: [QA](https://drive.google.com/open?id=1owi1kZAGqoL2qCqK4ItUX5_ovAqB8zSF)
* [Detailed Breakdown Of Product Specifications](https://drive.google.com/open?id=1M3vM8rGg_4sw84WRWIYT0k3hNCxfqQ4H)
* [Market Research](https://drive.google.com/open?id=1Ok9frSqm8EgFv1c8TRro6VO4et3BC2xo)
* Templates: [Test Case Template](https://drive.google.com/open?id=1Fwtq43O_ok9gFksgFJ1BlzlGHDZO2aRZH9NVeljyhDw)
* Templates: [Bug Report Teamplate](https://drive.google.com/open?id=1id71_FZqpCtgOgclIWxce239CdxjQzfZPa5eQ9ed6Dg)
* [Master Test Plan v3](https://drive.google.com/open?id=1_ULL_i56RKeNfjwnD19vIxfvdQPmDG05)
* [User Scenarios](https://drive.google.com/open?id=1fEfrsmWiXhTR3v5AAY3DWZiYa9jBFRks)
* List With Test Cases: [GDrive](https://drive.google.com/open?id=1gZ0oqvcji7B5gLLcluqilOjx0zWnEKI1uaV1icdNuio)
* List With Test Cases: [TestRail](https://sociableweaver.testrail.io/)
* [Database Review](https://drive.google.com/open?id=1EExkp7MeFd5QnuNUfY5Xe9vJ7cO16wzd)
* Supporting Documentation: [GitLab Repo Management](https://drive.google.com/open?id=1jIJYgnPmLAnguk4Ve68GlIsG81tT6yYo)
* Supporting Documentation: [Tasks And Time Estimation](https://drive.google.com/open?id=1Ss7_UOvlhRUn6alF3E7nZqwDSMmrQV80yeBdZ9-CtPk)
* [API Summary Report](https://drive.google.com/open?id=1knWJ6280g4wqQFgHlj8LxXCmzr_35VsD)
* [API Sample Test Run Reports](https://drive.google.com/open?id=1-PeQfW7APMGTh6zhzxKaMKrigui7ijHI)
* [UI Summary Report](https://drive.google.com/open?id=1Xs_hYP2unGhEJhY16gg0FLoi-wRM1Yd-)



[Archived Project documents](https://drive.google.com/open?id=1uI7UHD20POTIBO_Vu1rKevR1kx6vIGPP) can be located via the links below:

* [Master Test Plan v1](https://drive.google.com/open?id=1zcMOaQCHYR1OOmSgiqfHAIWjH97B-ipq)
* [Master Test Plan v2](https://drive.google.com/open?id=1L-DLNaCFdn7qVIy2C4Ym9lhKii_PpvSx)

