@ECHO OFF

SET current_location = %~dp0

cd %CD%
cd UI_Testing\UI_Framework
CALL mvn clean test -Dbrowser="firefox"
CALL allure serve "%CD%\target\surefire-reports"