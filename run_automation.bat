@ECHO OFF

SET current_location = %~dp0

cd %current_location%
call START load_test_data.bat
call START start_DEV_project.bat
timeout /t 60
call START run_postman_collections.bat
timeout /t 60
call START run_ui_automation.bat
EXIT