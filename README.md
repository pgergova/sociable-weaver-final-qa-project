<img align="right" src="Documentation/images/telerik_logo.PNG" alt="Telerik Academy Logo" />


<img align="center" src="Documentation/images/social_network.PNG" alt="Social Network Logo" />

<div align="center">

# SOCIABLE WEAVER
</div>

## TABLE OF CONTENTS

[I. INTRODUCTION](#i-introduction)

[II. MARCH TEAM MEMBERS](#ii-march-team-members)

[III. DELIVERABLES](#iii-deliverables)

[IV. USEFUL LINKS](#iv-useful-links)

[V. HOW TO RUN AUTOMATIONS](#v-how-to-run-automations)



## I. INTRODUCTION

__SOCIABLE WEAVER__ is a web application that allows you to connect and communicate with other people who enjoy watching birds. It enables you to:

- connect with other bird lovers by managing connection requests;
- manage your profile information;
- form discussions by creating, commenting and liking posts;
- get a feed of the newest/most relevant posts from people in your network.


[QA Project](https://drive.google.com/open?id=1owi1kZAGqoL2qCqK4ItUX5_ovAqB8zSF) was assigned by ScaleFocus.


[DEV Project](https://drive.google.com/open?id=1zr30YVZQFtjWFrJuNinfw-rfI6nEpuTu) was assigned by Upnetix.

## II. MARCH TEAM MEMBERS

### QA Team:
Petya Gergova<br />
Stanimir Dimitrov


### Development Team:
Georgi Georgiev<br />
Militsa Tsvetkova

## III. DELIVERABLES
1. [Master Test Plan v3.0](https://drive.google.com/open?id=1_ULL_i56RKeNfjwnD19vIxfvdQPmDG05)
2. Test Cases - [GDrive](https://drive.google.com/open?id=1gZ0oqvcji7B5gLLcluqilOjx0zWnEKI1uaV1icdNuio)
3. Test Cases - [TestRail](https://sociableweaver.testrail.io/index.php?/suites/view/1&group_by=cases:section_id&group_order=asc)
4. [UI Automation With Selenium And Java](https://gitlab.com/pgergova/sociable-weaver-final-qa-project/tree/master/UI_Testing)
5. [API Automation With Postman](https://gitlab.com/pgergova/sociable-weaver-final-qa-project/tree/master/API_Testing/Postman)
6. [API Summary Report](https://drive.google.com/open?id=1knWJ6280g4wqQFgHlj8LxXCmzr_35VsD)
7. [UI Summary Report](https://drive.google.com/open?id=1Xs_hYP2unGhEJhY16gg0FLoi-wRM1Yd-)
8. Market Research, Database Review and all supporting documentation can be located on [GDrive](https://drive.google.com/open?id=1GJ2SMRh9qsPOH2cecEZNvzbpBF4EeGER) and in the [Documentation](https://gitlab.com/pgergova/sociable-weaver-final-qa-project/tree/master/Documentation) folder of the repo

## IV. USEFUL LINKS
1. DEV Project - [GitLab](https://gitlab.com/pgergova/sociable-weaver-final-qa-project/tree/master/DEV_Project)
2. Bug Tracking System - [GitLab](https://gitlab.com/georgi.vesselinov.georgiev/final-project-telerik-java-may-2019-team-4/issues)
3. Full QA Documentation - [GitLab](https://gitlab.com/pgergova/sociable-weaver-final-qa-project/tree/master/Documentation)
4. Full QA Documentation - [GDrive](https://drive.google.com/open?id=1GJ2SMRh9qsPOH2cecEZNvzbpBF4EeGER)
5. Manual Test Runs - [TestRail](https://sociableweaver.testrail.io/)
6. QA Project Management Board - [Trello](https://trello.com/b/yJBliJsn/sociable-weaver-project)

## V. HOW TO RUN AUTOMATIONS

### __1. Pre-requisites for running automations__

To be able to run the project on localhost and run the test automations, the following programs need to be present on your machine:

- Windows 10 operating system
- JDK 8
- Java 1.8
- IntelliJ IDEA Ultimate Edition 2019.1.3 or later
- Gradle Command Line
- MariadDB v10.4.8 or later installed on localhost port 3306 with user "root",  password "password" and admin privileges
- Path to MariaDB bin folder should be added to PATH in system environment variables!
- Postman standalone v7.10.0 or later
- Newman v4.5.5 or later
- Newman HTML Extra Reporter  latest version
- Firefox latest version
- Selenium WebDriver
- Node.js v10.16.3 or later
- NPM v6.12.0 or later
- Apache Maven 3.6.0 or later
- Allure Command Line

### __2. Running the automations__

__NB:__ Prior to running the project and automations, make sure that localhost:8080 is not used, MaridDB is using port 3306 and is running. In addition, make sure that you do not have "social_network" db present, as well as user "socialnetwork'@'localhost".

1. Clone the repo on your computer.
2. Run run_automation.bat.

### __3. How to run DEV project individually__

__3.1. Manually via IntelliJ IDE__

1. Open IntelliJ Ultimate Edition.
2. Open DEV Project with Use Auto-Import option checked.
3. Build Project.
4. Connect MariadDB server on localhost:3306.
5. Create new schema "social_network".
6. Open script.sql in dbscript folder.
7. Select "social_network" as schema.
8. Select all in script.sql file.
9. Execute script.
10. Start application from DemoApplication class.

If above steps are followed correctly, you should have created a database named "social_network" and loaded test data in the database. The project should now be running on http://localhost:8080

__3.2. Via Batch File__
1. Click on load_test_data.bat on the Landing page of the repo (first make sure that db "social_network" and user "socialnetwork'@'localhost" are not present on MariaDB) - this will create the database, load the test data and create the user that is used for the connection between the project and the database.
2. Run start_DEV_project.bat on the Landing page of the repo.

### __4. How to run API tests automation individually__

1. Create the database and load test data as described in section 3.2.
2. Start Dev Application as described in section 3.2.
3. Run run_postman_collections.bat on the landing page of the repo.

After successful run of automation, HTML API reports should be available in a newly created folder "newman" at the root location of the repo.

__NB:__ If you would like to run only certain collection, you need to leave only the rows with the names of the collections in the run_postman_collections.bat file:

e.g. newman run API_Testing\Postman\Admin_Feature.postman_collection.json -e API_Testing\Postman\Sociable_Weaver_Live.postman_environment.json -g API_Testing\Postman\Sociable_Weaver.postman_globals.json -r htmlextra

### __5. How to run UI tests automation individually__

1. Create the database and load test data as described in section 3.2.
2. Start Dev Application as described in section 3.2.
3. Run run_ui_automation.bat file at the root location of the repo.

If automation runs fine, an Allure HTML report should be present at the end in your default browser.

__NB:__ If you would like to run a certain test suite, you need to add one additional parameter in the run_ui_automation.bat: -DrunSuite="Name of java class without the .class extension":

e.g. mvn clean test -Dbrowser="firefox" -DrunSuite="Test001_RegistrationNegativePaths"