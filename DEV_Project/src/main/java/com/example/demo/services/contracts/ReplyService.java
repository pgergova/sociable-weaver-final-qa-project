package com.example.demo.services.contracts;

import com.example.demo.models.Comment;
import com.example.demo.models.DTO.ReplyDTO;
import com.example.demo.models.Reply;
import com.example.demo.models.User;

import java.util.List;

public interface ReplyService {
    public List<Reply> getAllReplies();

    public Reply getReplyById(int id);

    public void deleteReply(int id, User user);

    public void createReply(Reply reply);

    public void updateReply(int id, ReplyDTO replyDTO);

    public int addReplyLike(int replyId, User user);

    public int removeReplyLike(int replyId, int userId);

    public String addRemoveAndCountReplyLike(int replyId, User user);

//    public int addRemoveAndCountReplyLike(int replyId, User user);

    //    public void createReply(Comment comment, User user, ReplyDTO replyDTO);
    public Reply generateReplyFromDTO(Comment comment, User user, ReplyDTO replyDTO);


    }
