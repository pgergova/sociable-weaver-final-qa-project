package com.example.demo.services.contracts;

import com.example.demo.models.Comment;
import com.example.demo.models.DTO.CommentDTO;
import com.example.demo.models.DTO.ReplyDTO;
import com.example.demo.models.Post;
import com.example.demo.models.User;

import java.util.List;

public interface CommentService {
    public List<Comment> getAllComments();

    public Comment getCommentById(int id);

    public void createComment(Comment comment);

    public void deleteComment(int id, User user);

    public void updateComment(int id, CommentDTO commentDTO);

    public int addCommentLike(int commentID, User user);

    public int removeCommentLike(int commentId, User user);

//     public void createReply(Comment comment, User user, ReplyDTO replyDTO);

    public String addRemoveAndCountCommentLike(int commentId, User user);

    void deleteAllRepliesOfComment(int commentId);

    public Comment generateCommentFromDTO(Post post, User user, CommentDTO commentDTO);
}
