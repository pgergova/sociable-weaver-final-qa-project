package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Base64;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

//    @Column(name = "username",nullable = false, unique = true)
//    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @Size(min=1, max = 20)
    @Column(name = "first_name")
    private String firstName;

    @Size(min=1, max = 20)
    @Column(name = "last_name")
    private String lastName;

    @Email
    @Pattern(regexp = "[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
    @Column(name = "email",nullable = false, unique = true)
    private String email;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "city")
    private String city;

    @Lob
    @Column(name = "photo",columnDefinition = "MEDIUMBLOB")
    private byte[] photo;

    @Column(name = "shared")
    private boolean shared =false;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade={CascadeType.ALL})
    @JoinTable(name = "friend_requests",
        joinColumns = {@JoinColumn(name = "user_sender_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_receiver_id")}
    )
    private Set<User> sentFriendRequests;

    @JsonIgnore
    @ManyToMany(mappedBy = "sentFriendRequests", fetch = FetchType.EAGER)
    private Set<User> receivedFriendRequests;

//    @Column(name = "password_confirmation")
//    private String passwordConfirmation;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "friendships",
        joinColumns = {@JoinColumn(name = "user_friend1_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_friend2_id")}
    )
    private Set<User> friendListAsRequester;

    @JsonIgnore
    @ManyToMany(mappedBy = "friendListAsRequester", fetch = FetchType.EAGER)
    private Set<User> friendListAsAccepter;

    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Collection<Role> roles;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Set<Post> posts;


    public User() {
    }

    public User(String password, String firstName, String lastName, String email) {
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        sentFriendRequests=new HashSet<>();
        receivedFriendRequests=new HashSet<>();
        friendListAsRequester=new HashSet<>();
        friendListAsAccepter=new HashSet<>();
        posts=new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoto() {
        if(photo!=null){
            return Base64.getEncoder().encodeToString(photo);
        }
        return null;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }
//
//    public String getPasswordConfirmation() {
//        return passwordConfirmation;
//    }
//
//    public void setPasswordConfirmation(String passwordConfirmation) {
//        this.passwordConfirmation = passwordConfirmation;
//    }


    public Set<User> getSentFriendRequests() {
        return sentFriendRequests;
    }

    public void setSentFriendRequests(Set<User> sentFriendRequests) {
        this.sentFriendRequests = sentFriendRequests;
    }

    public Set<User> getReceivedFriendRequests() {
        return receivedFriendRequests;
    }

    public void setReceivedFriendRequests(Set<User> receivedFriendRequests) {
        this.receivedFriendRequests = receivedFriendRequests;
    }

    public Set<User> getFriendListAsRequester() {
        return friendListAsRequester;
    }

    public void setFriendListAsRequester(Set<User> friendListAsRequester) {
        this.friendListAsRequester = friendListAsRequester;
    }

    public Set<User> getFriendListAsAccepter() {
        return friendListAsAccepter;
    }

    public void setFriendListAsAccepter(Set<User> friendListAsAccepter) {
        this.friendListAsAccepter = friendListAsAccepter;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public Set<Post> getPosts() {
        return posts;

    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }


    public byte[] getPhotoBytes() {
        return photo;
    }


}
