package com.example.demo.repositories;

import com.example.demo.models.Post;
import com.example.demo.models.Reply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReplyRepository extends JpaRepository<Reply,Integer> {

    @Query(value = "select * from replies r join users u on r.user_id=u.user_id where u.enabled=1",nativeQuery = true)
     List<Reply> findAllByActiveUser();
}
