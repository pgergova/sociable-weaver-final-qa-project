package com.example.demo.controllers.rest;

import com.example.demo.exceptions.*;
import com.example.demo.models.DTO.PostDTO;
import com.example.demo.models.Post;
import com.example.demo.models.User;
import com.example.demo.services.contracts.PostService;
import com.example.demo.services.contracts.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;

@RequestMapping("/api/v1/posts")
@RestController
public class PostRestController {
    private PostService postService;
    private UserService userService;


    public PostRestController(PostService postService, UserService userService) {
        this.postService = postService;
        this.userService = userService;
    }


    @GetMapping("/all/pageable")
    public Page<Post> getAllPostsPageable(Pageable pageable, Authentication principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            return postService.getAllPostsPageable(pageable, user);
        }catch (EntityForbiddenRequestException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Post getPostById(@PathVariable int id){
        try {
            return postService.getPostById(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

//    @PostMapping
//    public void createPost(@Valid @RequestBody Post post){
//        try {
//            postService.createPost(post);
//        }catch (EntityBadRequestException e){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//        }
//    }


    @PutMapping("/{id}")
    public void updatePost(@PathVariable int id,@Valid @RequestBody PostDTO postDTO, Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            postService.updatePost(id,postDTO, user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityBadRequestException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch (EntityForbiddenRequestException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PutMapping("/privacy/{id}")
    public void changePrivacy(@PathVariable int id,@Valid @RequestBody PostDTO postDTO, Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            postService.chancePrivacy(id,postDTO, user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityAlreadyChangedException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch (EntityForbiddenRequestException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }




    @PutMapping("/privacy/change/{postId}")
    public String changePostPrivacy(@PathVariable int postId, Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            return postService.changePostPrivacy(postId, user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityForbiddenRequestException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }



    @PostMapping("new")
    public void createPost(@Valid @RequestBody PostDTO postDTO, Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            postService.createPost(postService.generatePostFromDTO(user,postDTO));
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityBadRequestException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{postId}/add_likes")
    public int addPostLike(@PathVariable int postId,
                               Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            return postService.addPostLike(postId,user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityAlreadyExistsException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

//        return size;
    }


    @PutMapping("/{postId}/likes")
    public String addRemoveAndCountPostLike(@PathVariable int postId,
                            Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        return postService.addRemoveAndCountPostLike(postId,user);
    }

    @DeleteMapping("/{postId}/likes")
    public int removePostLike(@PathVariable int postId,
                                  Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            return postService.removePostLike(postId,user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deletePost(@PathVariable int id, Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        try {
            postService.deletePost(id, user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityForbiddenRequestException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }
}
