package com.example.demo.exceptions;

public class EntityForbiddenRequestException extends RuntimeException{
    public EntityForbiddenRequestException(String message) {
        super(message);
    }
}
