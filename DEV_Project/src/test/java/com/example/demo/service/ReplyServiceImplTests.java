package com.example.demo.service;

import com.example.demo.exceptions.EntityAlreadyExistsException;
import com.example.demo.exceptions.EntityBadRequestException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Comment;
import com.example.demo.models.DTO.ReplyDTO;
import com.example.demo.models.Post;
import com.example.demo.models.Reply;
import com.example.demo.models.User;
import com.example.demo.repositories.ReplyRepository;
import com.example.demo.services.ReplyServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
public class ReplyServiceImplTests {

    @Mock
    ReplyRepository mockReplyRepository;

    @InjectMocks
    ReplyServiceImpl replyService;

    @Before

    @Test
    public void get_ReplyById_Should_Return_A_Reply_When_Id_Matches(){

        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);
        Mockito.when(mockReplyRepository.findById(1))
                .thenReturn(Optional.of(reply));

        // Act
        Reply replyForTest = replyService.getReplyById(1);

        // Assert
        Assert.assertEquals("replyText", replyForTest.getReplyText());
    }





    @Test(expected = EntityNotFoundException.class)
    public void get_ReplyById_Should_Throw_An_Exception_When_Id_NotExists(){

        // Arrange

//        User user=new User("pass1","fName1","lName1","email1");
//        Post post=new Post("postText",user,true);
//        Comment comment=new Comment(post,user,"commentText");
//        Reply reply1 =new Reply("replyText1",comment,user);
//        Reply reply2 =new Reply("replyText2",comment,user);
//        Reply reply3 =new Reply("replyText3",comment,user);
//
//        Mockito.lenient().when(mockReplyRepository.findAll())
//                .thenReturn(Arrays.asList(reply1,reply2,reply3));

        // Act
        Reply replyForTest = replyService.getReplyById(4);
    }



    @Test
    public void createReply_Should_Invoke_Repository_Save() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);

        // Act
        replyService.createReply(reply);
        // Assert
        Mockito.verify(mockReplyRepository, Mockito.times(1)).save(reply);
        Assert.assertEquals("replyText", replyService.getReplyById(1).getReplyText());

    }

    @Test(expected = EntityBadRequestException.class)
    public void createReply_Should_Throw_An_Exception_When_TextEmpty() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("",comment,user);

        // Act
        replyService.createReply(reply);

    }


    @Test
    public void deleteReply_Should_Invoke_Repository_Delete() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);
        Mockito.when(mockReplyRepository.findById(1))
                .thenReturn(Optional.of(reply));

        // Act
        replyService.deleteReply(1);
        // Assert
        Mockito.verify(mockReplyRepository, Mockito.times(1)).deleteById(1);
    }


    @Test(expected = EntityBadRequestException.class)
    public void updateReply_Should_Throw_An_Exception_When_TextEmpty(){

        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply1 =new Reply("",comment,user);
        ReplyDTO replyDTO =new ReplyDTO();

        Mockito.lenient().when(mockReplyRepository.findById(1))
                .thenReturn(Optional.of(reply1));
        // Act
        replyService.updateReply(1,replyDTO);
    }

    @Test
    public void updateReply_Should_Invoke_Repository_Save() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);
        ReplyDTO replyDTO =new ReplyDTO("newReplyText");
        Mockito.when(mockReplyRepository.findById(1))
                .thenReturn(Optional.of(reply));

        // Act
        replyService.updateReply(1,replyDTO);
        // Assert
        Mockito.verify(mockReplyRepository, Mockito.times(1)).save(reply);
        Assert.assertEquals("newReplyText", reply.getReplyText());
    }


    @Test
    public void get_AllReplies_Should_Return_A_List_Of_Replies() {

        //Arrange
        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
//        Reply reply1 =new Reply("replyText1",comment,user);

        Mockito.when(mockReplyRepository.findAllByActiveUser())
                .thenReturn(Arrays.asList(
                        new Reply("replyText1",comment,user)));
        //Act

        List<Reply>replies=replyService.getAllReplies();

        //Assert
        Assert.assertEquals(1, replies.size());
    }



    @Test
    public void addReplyLike_Should_Invoke_Repository_Save_And_Give_CorrectCount() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);
        Mockito.when(mockReplyRepository.findById(1))
                .thenReturn(Optional.of(reply));

        // Act
        replyService.addReplyLike(1,user);
        // Assert
        Mockito.verify(mockReplyRepository, Mockito.times(1)).save(reply);
        Assert.assertEquals(1, reply.getReplyLikes().size());
    }

    @Test(expected = EntityAlreadyExistsException.class)
    public void addReplyLike_Should_Throw_An_Exception_When_ReplyLikeExists() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);
        reply.getReplyLikes().add(user);
        Mockito.when(mockReplyRepository.findById(1))
                .thenReturn(Optional.of(reply));

        // Act
        replyService.addReplyLike(1,user);
        }



    @Test
    public void removeReplyLike_Should_Invoke_Repository_Save_And_Give_CorrectCount() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);
        replyService.addReplyLike(1,user);
        Mockito.when(mockReplyRepository.findById(1))
                .thenReturn(Optional.of(reply));
        reply.getReplyLikes().add(user);

        // Act
        replyService.removeReplyLike(1,user.getId());
        // Assert
        Mockito.verify(mockReplyRepository, Mockito.times(1)).save(reply);
        Assert.assertEquals(0, reply.getReplyLikes().size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeReplyLike_Should_Throw_An_Exception_When_ReplyLikeExists() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);
        Mockito.when(mockReplyRepository.findById(1))
                .thenReturn(Optional.of(reply));

        // Act
        replyService.removeReplyLike(1,user.getId());
    }


    @Test
    public void addRemoveAndCountReplyLike_Should_Invoke_Repository_Save_And_Give_CorrectCount() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);
        Mockito.when(mockReplyRepository.findById(1))
                .thenReturn(Optional.of(reply));

        // Act
        replyService.addRemoveAndCountReplyLike(1,user);
        replyService.addRemoveAndCountReplyLike(1,user);
        // Assert
        Mockito.verify(mockReplyRepository, Mockito.times(2)).save(reply);
        Assert.assertEquals(0, reply.getReplyLikes().size());
    }



    @Test(expected = EntityBadRequestException.class)
    public void generateReplyFromDTO_Throws_Exception_When_ReplyTextEmpty() {
        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        ReplyDTO replyDTO =new ReplyDTO();

        // Act
        replyService.generateReplyFromDTO(comment,user,replyDTO);

    }

    @Test
    public void generateReplyFromDTO_Should_Invoke_Repository_Save() {
        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        ReplyDTO replyDTO =new ReplyDTO("replyText");

        // Act
        Reply replyForTest= replyService.generateReplyFromDTO(comment,user,replyDTO);

        // Assert
//        Mockito.verify(mockReplyRepository, Mockito.times(1)).save(new Reply(replyDTO.getReplyText(), comment, user));
        Assert.assertEquals("replyText", replyForTest.getReplyText());


    }


}
