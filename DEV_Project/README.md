                                                              **Sociable Weaver**
                    
Web application that allows you to connect with other people who enjoy watching birds, collaborate by sharing field notes and photos, receive activity posts from people in your network, form discussions in the manner of commenting under someone's post, share observations and learn more

Project Assigned by Upnetix

________________________________________________________________________________
**Team Members**

Development Team:

Georgi Georgiev

Militsa Tsvetkova

QA Team:

Petya Gergova

Stanimir Dimitrov 

________________________________________________________________________________
**Technology Stack**

Main

Front end: JavaScript, HTML and CSS

Back end: Spring MVC Framework with Thymeleaf template engine https://spring.io/

Database: MariaDB https://mariadb.org/


Utility libraries

Mockito: https://site.mockito.org/

Spring Data JPA: https://spring.io/projects/spring-data-jpa

Spring Boot built in libraries

GreenMail: http://www.icegreen.com/greenmail/

Jasypt: http://www.jasypt.org/

ZXCVBN: https://github.com/dropbox/zxcvbn

Java Mail API: https://javaee.github.io/javamail/

Swagger: https://swagger.io/

________________________________________________________________________________
**Design Principles**

• OOP principles

• KISS, SOLID, DRY principles

• REST API design best practices

• BDD in tests writing

________________________________________________________________________________
Trello Board: https://trello.com/b/ZJ5YB2ir/social-network
