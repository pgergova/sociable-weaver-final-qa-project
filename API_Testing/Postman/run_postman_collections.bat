@ECHO OFF

SET current_location = %~dp0

cd %current_location%
ECHO "Running Admin Feature Tests"
call newman run Admin_Feature.postman_collection.json -e Sociable_Weaver_Live.postman_environment.json -g Sociable_Weaver.postman_globals.json -r htmlextra

ECHO "Running Posts Feature Tests"
call newman run Posts_Feature.postman_collection.json -e Sociable_Weaver_Live.postman_environment.json -g Sociable_Weaver.postman_globals.json -r htmlextra

ECHO "Running Comments Feature Tests"
call newman run Comments_Feature.postman_collection.json -e Sociable_Weaver_Live.postman_environment.json -g Sociable_Weaver.postman_globals.json -r htmlextra

ECHO "Running Profile Management Feature Tests"
call newman run Profile_Management_Feature.postman_collection.json -e Sociable_Weaver_Live.postman_environment.json -g Sociable_Weaver.postman_globals.json -r htmlextra

ECHO "Running User Connections Feature Tests"
call newman run User_Connections_Feature.postman_collection.json -e Sociable_Weaver_Live.postman_environment.json -g Sociable_Weaver.postman_globals.json -r htmlextra