CREATE OR REPLACE DATABASE social_network;

use social_network;

create table roles
(
    role_id int auto_increment
        primary key,
    name    varchar(25) null
);

create table users
(
    user_id     int auto_increment
        primary key,
    first_name  varchar(20)       null,
    last_name   varchar(20)       null,
    email       varchar(50)       not null,
    password    varchar(255)      null,
    enabled     tinyint           not null,
    nationality varchar(50)       null,
    city        varchar(50)       null,
    photo       mediumblob        null,
    shared      tinyint default 0 not null,
    constraint username
        unique (email)
);


create table friend_requests
(
    friend_request_id int auto_increment
        primary key,
    user_sender_id    int not null,
    user_receiver_id  int not null,
    constraint sent_requests_users_user_id_fk
        foreign key (user_sender_id) references users (user_id),
    constraint sent_requests_users_user_id_fk_2
        foreign key (user_receiver_id) references users (user_id)
);

create table friendships
(
    friendship_id   int auto_increment
        primary key,
    user_friend1_id int not null,
    user_friend2_id int not null,
    constraint friendships_users_user_id_fk
        foreign key (user_friend1_id) references users (user_id),
    constraint friendships_users_user_id_fk_2
        foreign key (user_friend2_id) references users (user_id)
);

create table confirmationtoken
(
    token_id           int auto_increment
        primary key,
    confirmation_token varchar(255) null,
    created_date       datetime     null,
    user_id            int          null,
    expire_date        datetime     null,
    constraint confirmationtoken_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table password_reset_token
(
    token_id    int auto_increment
        primary key,
    token       varchar(255) default '' not null,
    create_date datetime                not null,
    user_id     int          default 0  not null,
    expire_date datetime                null,
    constraint FK_password_reset_token_users
        foreign key (user_id) references users (user_id)
);

create table posts
(
    post_id    int auto_increment
        primary key,
    user_id    int               not null,
    post_text  text              not null,
    post_photo mediumblob        null,
    shared     tinyint default 0 not null,
    created_at datetime          not null,
    updated_at datetime          not null,
    constraint posts_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table comments
(
    comment_id   int auto_increment
        primary key,
    comment_text text     not null,
    post_id      int      not null,
    user_id      int      not null,
    created_at   datetime not null,
    updated_at   datetime not null,
    constraint comments_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint comments_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table comment_likes
(
    comment_id int not null,
    user_id    int not null,
    constraint comment_likes_comments_comment_id_fk
        foreign key (comment_id) references comments (comment_id),
    constraint comment_likes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table post_likes
(
    like_id int auto_increment
        primary key,
    post_id int not null,
    user_id int not null,
    constraint likes_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint likes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table replies
(
    reply_id   int auto_increment
        primary key,
    reply_text text     not null,
    comment_id int      null,
    user_id    int      not null,
    created_at datetime not null,
    updated_at datetime not null,
    constraint replies_comments_comment_id_fk
        foreign key (comment_id) references comments (comment_id),
    constraint replies_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table reply_likes
(
    reply_id int not null,
    user_id  int not null,
    constraint reply_likes_replies_reply_id_fk
        foreign key (reply_id) references replies (reply_id),
    constraint reply_likes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table user_roles
(
    user_id int null,
    role_id int null,
    constraint user_roles_roles_role_id_fk
        foreign key (role_id) references roles (role_id),
    constraint user_roles_users_user_id_fk
        foreign key (user_id) references users (user_id)
);


CREATE OR REPLACE USER 'socialnetwork'@'localhost' IDENTIFIED BY 'pass';
GRANT USAGE ON *.* TO 'socialnetwork'@'localhost';
GRANT ALL PRIVILEGES ON `social_network`.* TO 'socialnetwork'@'localhost';
FLUSH PRIVILEGES;

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `enabled`, `nationality`, `city`, `photo`, `shared`) VALUES
	(1, 'Admin', 'User', 'sociable_weaver@abv.bg', '$2a$10$QojFVxZSO6t0COWaSYpRVeULUeNNtQUccd3UScNY1F6XGb3XYsOV.', 1, NULL, NULL, NULL, 0),
	(2, 'Test', 'User', 'sw_user@abv.bg', '$2a$10$R8.zKgNfsnUt8p.I/671Ieycy4/sE6W85FyBT5oa3eQ83squcOhXy', 1, NULL, NULL, NULL, 0),
	(3, 'Test', 'User 2', 'sw_user2@abv.bg', NULL, 0, NULL, NULL, NULL, 0),
	(4, 'Admin', 'User 2', 'pgqa_test@abv.bg', '$2a$10$3Vw13qIXe4lwLBNu78gmreRWudELzm6aMTyvb7/gs0kkULergFLi.', 0, NULL, NULL, NULL, 0),
	(5, 'Test', 'User 3', 'sw_user3@abv.bg', '$2a$10$XC0TTrFLg7RCjq1.UDJ8AeFeB91VmiNyQgC2RDr/Ro.sc18zpI6Q.', 1, NULL, NULL, NULL, 0),
	(6, 'Test', 'User 4', 'sw_user4@abv.bg', '$2a$10$kCC6Fmre49tbbI6Sv9mgZeT1UAiFuV.wPirW5kBD.vL6QDw8qfah.', 0, NULL, NULL, NULL, 0),
	(7, 'Test', 'User 5', 'sw_user5@abv.bg', '$2a$10$7BpB5.wBuBYNzRerLbg2lOjncShNW76dVOG8K27/x7gulbiWBGJrW', 1, NULL, NULL, NULL, 0),
	(8, 'Test', 'User 6', 'sw_user6@abv.bg', '$2a$10$zaUmOZwPSdpPnU658MwrVOip2N/EtUKPWGeO0rhQ70WdNu5Z7KLUC', 1, NULL, NULL, NULL, 1);

INSERT INTO `confirmationtoken` (`token_id`, `confirmation_token`, `created_date`, `user_id`, `expire_date`) VALUES
	(1, '9d9dff14-3608-4345-8794-f763ff02511b', '2019-11-20 19:29:36', 1, NULL),
	(2, 'f511cb6a-16a5-4316-a108-a745dcd5b5a2', '2019-11-20 19:32:36', 2, NULL),
	(3, 'ee02e48c-69d3-4477-82c5-20440af63c09', '2019-11-20 19:34:19', 3, NULL),
	(4, 'ae89d2c9-38e9-42b5-ad5d-325c4d167001', '2019-11-20 19:35:03', 4, NULL),
	(5, 'c84404c9-c8bc-47f5-af3b-10489f1ba367', '2019-11-21 10:54:39', 5, NULL),
	(6, 'f61dd680-32b4-44ed-b692-62d6bfbca2c2', '2019-11-26 23:27:11', 6, NULL),
	(7, '31ccf425-0920-4e43-81d6-138ca8d46b09', '2019-11-26 23:27:24', 7, NULL),
	(8, '2643cbfe-4d78-4118-a23b-c8484bd8f5ef', '2019-12-02 01:50:14', 8, NULL);

INSERT INTO `roles` (`role_id`, `name`) VALUES
	(1, 'ROLE_ADMIN'),
	(2, 'ROLE_USER'),
	(3, 'ROLE_USER'),
	(4, 'ROLE_ADMIN'),
	(5, 'ROLE_USER'),
	(6, 'ROLE_USER'),
	(7, 'ROLE_USER'),
	(8, 'ROLE_USER');

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
	(1, 1),
	(2, 2),
	(3, 3),
	(4, 4),
	(5, 5),
	(6, 6),
	(7, 7),
	(8, 8);

INSERT INTO `friendships` (`friendship_id`, `user_friend1_id`, `user_friend2_id`) VALUES
	(1, 2, 5);