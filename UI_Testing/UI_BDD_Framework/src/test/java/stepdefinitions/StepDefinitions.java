package stepdefinitions;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element){
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field){
        actions.typeValueInField(value, field);
    }

    @Given("I go to landing page")
    public void goToLandingPage(){
        actions.goToLandingPage();
    }

    @Given("I am on the Login page")
    public void goToLoginPage(){actions.goToLogInPage();}

    @Given("I am on the Registration page")
    public void goToRegistrationPage() {
        actions.goToTheRegisterPage();
    }

    @When("I try logging in with $email and $password")
    public void logInWithCredentials(String email, String password){
        actions.waitForElementVisible("loginPage.emailInput", 800);
        actions.typeValueInField(Utils.getConfigPropertyByKey(email), "loginPage.emailInput");
        actions.waitForElementVisible("loginPage.passwordInput", 800);
        actions.typeValueInField(Utils.getConfigPropertyByKey(password), "loginPage.passwordInput");
        actions.waitForElementVisible("loginPage.logInButton", 800);
        actions.clickElement("loginPage.logInButton");
    }

    @When("I enter $firstName, $lastName and $email")
    public void registerWithInvalidInformation(String firstName, String lastName, String email) {
        actions.registerWithInvalidInformation(firstName, lastName, email);
    }

    @Then ("I am redirected to the Home page")
    public void assertRedirectionToHomePage() {
        actions.waitForURLToContainText("homepage", 800);
        actions.assertElementPresent("homePage.homePageLink");
    }

    @Then ("I receive a $message")
    public void assertAlertMessageIsReceived(String message) {
        actions.waitForElementVisible("loginPage.alertContainer", 800);
        actions.assertElementContainsText(Utils.getConfigPropertyByKey(message), "loginPage.alertContainer");
    }

    @Then("The Register button is disabled")
    public void assertRegisterButtonIsDisabled() {
        actions.assertElementAttributeContainsText("disabled", "registerPage.registerButton", "class");
    }

    @Then("I receive an alert for already registered email")
    public void clickOnRegisterButtonAndReceiveAlert() {
        actions.waitForElementClickable("registerPage.registerButton", 800);
        actions.clickElement("registerPage.registerButton");
        actions.waitForElementVisible("registerPage.alertContainer", 800);
        actions.assertElementContainsText(Utils.getConfigPropertyByKey("alreadyRegisteredUserAlertMessage"), "registerPage.alertContainer");
    }
}
