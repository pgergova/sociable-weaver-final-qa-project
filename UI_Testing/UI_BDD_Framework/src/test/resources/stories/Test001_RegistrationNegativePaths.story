Meta:
@registration

Narrative:
As an unregistered user of Sociable Weaver,
I try registering with invalid informaiton
so that I can verify that the system will not allow me to register.

Scenario: As an unregistered user of Sociable Weaver, I try registering with blank first name
Given I am on the Registration page
When I enter blankFirstName, validLastName and userEmail
Then The Register button is disabled

Scenario: As an unregistered user of Sociable Weaver, I try registering with blank last name
Given I am on the Registration page
When I enter validFirstName, blankLastName and userEmail
Then The Register button is disabled


Scenario: As an unregistered user of Sociable Weaver, I try registering with blank email
Given I am on the Registration page
When I enter validFirstName, validLastName and blankEmail
Then The Register button is disabled


Scenario: As an unregistered user of Sociable Weaver, I try registering with invalid email
Given I am on the Registration page
When I enter validFirstName, validLastName and invalidEmail
Then The Register button is disabled


Scenario: As an unregistered user of Sociable Weaver, I try registering with too long email
Given I am on the Registration page
When I enter validFirstName, validLastName and tooLongEmailAddress
Then The Register button is disabled


Scenario: As an unregistered user of Sociable Weaver, I try registering with an already registered email
Given I am on the Registration page
When I enter validFirstName, validLastName and userEmail
Then I receive an alert for already registered email