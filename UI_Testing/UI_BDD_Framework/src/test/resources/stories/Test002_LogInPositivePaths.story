Meta:
@logIn

Narrative:
As a registered user of Sociable Weaver
I want to log in
So that I can get to the Home page.

Scenario: As a registered user of Sociable Weaver, I want to log in with correct credentials
Given I am on the Login page
When I try logging in with userEmail and userPassword
Then I am redirected to the Home page