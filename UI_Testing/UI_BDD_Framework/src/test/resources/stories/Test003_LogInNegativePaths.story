Meta:
@logIn

Narrative:
As a registered user of Sociable Weaver,
I try logging in with invalid credentials,
so that I can verify that the system will not let me in.

Scenario: As a registred user of Sociable Weaver, I try logging in with correct email and incorrect password
Given I am on the Login page
When I try logging in with userEmail and incorrectPassword
Then I receive a badCredentialsAlertMessage

Scenario: As a registered user of Sociable Weavver, I try logging in with incorrect email annd correct password
Given I am on the Login page
When I try logging in with incorrectEmail and userPassword
Then I receive a badCredentialsAlertMessage

Scenario: As a registered user of Sociable Weaver, I try logging in by leaving email field blank
Given I am on the Login page
When I try logging in with blankEmail and userPassword
Then I receive a badCredentialsAlertMessage

Scenario: As a registered user of Sociable Weaver, I try logging in by leaving the password field blank
Given I am on the Login page
When I try logging in with userEmail and blankPassword
Then I receive a badCredentialsAlertMessage

Scenario: As a registered user of Sociable Weaver, I try logging into a disabled account
Given I am on the Login page
When I try logging in with disabledEmailAddress and userPassword
Then I receive a disabledAccountAlertMessage
