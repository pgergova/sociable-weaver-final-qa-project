package com.telerikacademy.uiframework;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.remote.SessionId;


public class CustomWebDriverManager {
	public enum CustomWebDriverManagerEnum {
		INSTANCE;
		private static String browser = System.getProperty("browser");
		private static WebDriver driver = setupBrowser();
        /*
		// Run tests via the IDE with Firefox and WebDriverManager v2.0.0
		private static WebDriver setupBrowser() {
			FirefoxDriverManager.getInstance().setup();
			WebDriver firefoxDriver = new FirefoxDriver();
			firefoxDriver.manage().window().maximize();
			return firefoxDriver;
		}*/

		/*
		// Run tests via the IDE with Chrome and WebDriverManager v2.0.0
		private WebDriver setupBrowser() {
			ChromeDriverManager.getInstance().version("78.0.3904").setup();
			WebDriver chromeDriver = new ChromeDriver();
			chromeDriver.manage().window().maximize();
			driver = chromeDriver;
			return driver;
		}*/
	/*

		// Choose a browser when running tests with the command line and WebDriverManager v2.0.0
		private WebDriver setupBrowser(String userBrowser) {
			if (userBrowser.equalsIgnoreCase("chrome")) {
				ChromeDriverManager.getInstance().setup();
				WebDriver chromeDriver = new ChromeDriver();
				chromeDriver.manage().window().maximize();
				driver = chromeDriver;
			} else if (browser.equalsIgnoreCase("firefox") || browser.equalsIgnoreCase("mozilla") || browser.equalsIgnoreCase("")) {
				FirefoxDriverManager.getInstance().setup();
				WebDriver firefoxDriver = new FirefoxDriver();
				firefoxDriver.manage().window().maximize();
				driver = firefoxDriver;
			} else {
				System.out.println("We currently support only Chrome and Firefox");
			}
			return driver;
		}

	*/

		// Use Firefox via the IDE with WebDrierManager v3.0.0

        private static WebDriver setupBrowser() {
            WebDriverManager.firefoxdriver().setup();
            WebDriver firefoxDriver = new FirefoxDriver();
            firefoxDriver.manage().window().maximize();
            return firefoxDriver;
        }

	/* // Use Chrome via the IDE with WebDriverManager v3.0.0
    private WebDriver setupBrowser() {
            WebDriverManager.chromedriver().version("78.0.3904").setup();
            WebDriver chromeDriver = new ChromeDriver();
            chromeDriver.manage().window().maximize();
            driver = chromeDriver;
    } */

    // Choose the browser via the command line
    private static WebDriver setupBrowser(String userBrowser) {
        if(userBrowser.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            WebDriver chromeDriver = new ChromeDriver();
            chromeDriver.manage().window().maximize();
            driver = chromeDriver;
        }

        else if(userBrowser.equalsIgnoreCase("firefox") || userBrowser.equalsIgnoreCase("mozilla") || userBrowser.equalsIgnoreCase("")) {
            WebDriverManager.firefoxdriver().setup();
            WebDriver firefoxDriver = new FirefoxDriver();
            firefoxDriver.manage().window().maximize();
            driver = firefoxDriver;
        }

        else {
            System.out.println("We currently support only Chrome and Firefox");
        }

        return driver;
    }

		public void quitDriver() {
			if (driver != null) {
				driver.quit();
			}
		}

		public WebDriver getDriver() {

            /*
            // Condition when the browser is taken from the command line
            SessionId session;
            if(browser.equalsIgnoreCase("firefox") || browser.equalsIgnoreCase("mozilla")) {
                session = ((FirefoxDriver)driver).getSessionId();
            }

            else {
                session = ((ChromeDriver)driver).getSessionId();
            }


			if(session==null)
			{
				driver = setupBrowser(browser);
			}
			return driver;*/


            // Condition when firefox is used via the IDE
            SessionId session = ((FirefoxDriver)driver).getSessionId();

			if(session==null)
			{
				driver = setupBrowser();
			}
			return driver;
		}
	}
}
