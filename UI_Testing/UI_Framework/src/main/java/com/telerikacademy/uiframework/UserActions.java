package com.telerikacademy.uiframework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class UserActions {
	final WebDriver driver;

	public UserActions() {
		this.driver = Utils.getWebDriver();
	}

	public static void loadBrowser() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
	}

	public static void quitDriver(){
		Utils.tearDownWebDriver();
	}

	//############# DRIVER ACTIONS #########

	public WebElement findElement(String locator) {
		Utils.LOG.info("Finding web element " + locator);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		return element;
	}

	public void clickElement(String key){
		Utils.LOG.info("Clicking on element " + key);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
	}

	public void typeValueInField(String value, String field){
		Utils.LOG.info("Typing in " + field);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		clearTextInField(field);
		element.sendKeys(value);
	}

	public void clearTextInField(String locator) {
		Utils.LOG.info("Clearing preloaded content in field " + locator);
		findElement(locator).clear();
	}

	public int getSizeOfElements(String locator) {
		int numberOfElements = driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size();
		return numberOfElements;
	}

	//############# ASSERTS #########

	public void assertElementPresent(String locator){
		Utils.LOG.info("Asserting that element "+ locator+ " is present on the page");
		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public void assertElementNotPresent(String locator) {
		Utils.LOG.info("Asserting that element "+ locator+ " is NOT present on the page");
		Utils.LOG.info("Element "  + locator + " does NOT exist: ");
		Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
	}

	public void assertElementIsNotVisible(String locator) {
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		Assert.assertFalse(element.isDisplayed());
	}

	public void assertElementContainsText(String text, String locator) {
		Utils.LOG.info("Asserting that element " + locator + " contains text: " + text);
		String textOfPost = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
		Assert.assertEquals(text, textOfPost);
	}

	public void assertElementDoesNotContainText(String text, String locator) {
		Utils.LOG.info("Asserting that element " + locator + " does NOT contain text: " + text);
		String textOfPost = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
		Assert.assertNotEquals(text, textOfPost);
	}

	public void assertElementAttributeContainsText(String text, String locator, String attribute) {
		Utils.LOG.info("Asserting that element's " + locator + " attribute " + attribute + " contains text: " + text);
		boolean result = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getAttribute(attribute).contains(text);
		Assert.assertTrue(result);
	}

	public void assertNumbersAreEqual(int elementOn, int elementTwo) {
		Assert.assertTrue(elementOn == elementTwo);
	}

	public void assertStringsAreSame(String textOne, String textTwo) {
		Utils.LOG.info("Asserting Strings are the same");
		Assert.assertTrue(textOne.equals(textTwo));
	}

	public void assertStringsAreNotSame(String textOne, String textTwo) {
		Utils.LOG.info("Asserting Strings are not same");
		Assert.assertFalse(textOne.equals(textTwo));
	}

	public void assertRequestFriendshipLinkIsNotPresent(String currentUserNames, String userLocator, String linkLocator){

		goToUserProfile(currentUserNames, userLocator);
		assertElementPresent(linkLocator);
	}

	//############# WAITS #########

	public void waitForElementVisible(String locator, int seconds){
		Utils.LOG.info("Waiting for element " + locator + " to be visible");
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public void waitForElementClickable(String locator, int seconds) {
		Utils.LOG.info("Waiting for element " + locator + " to be clickable");
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public void waitForTextToBePresentInElement(String text, String locator, int seconds) {
		Utils.LOG.info("Waiting for text " + text + " to be present in element " + locator);
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		wait.until(ExpectedConditions.textToBePresentInElement(element, text));
	}

	public void waitForElementNotPresent(String locator, int seconds) {
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public void waitForTextToNotBePresentInElement(String text, String locator, int seconds) {
		Utils.LOG.info("Waiting for text " + text + " to not be present in element " + locator);
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		//WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		wait.until(ExpectedConditions.textToBe(By.xpath(Utils.getUIMappingByKey(locator)), text));
		//.invisibilityOfElementWithText(By.xpath(Utils.getUIMappingByKey(locator)), text)
	}

	// ############# USER ACTIONS #########
	public void goToLandingPage() {
		Utils.LOG.info("Going to Landing page");
		driver.get(Utils.getConfigPropertyByKey("base.url"));
	}

	public void goToLogInPage(){
		goToLandingPage();
		Utils.LOG.info("Going to Log in page");
		waitForElementVisible("landing.logIn", 800);
		clickElement("landing.logIn");
	}

	public void goToTheRegisterPage() {
		Utils.LOG.info("Going to Register page");
		goToLogInPage();
		waitForElementClickable("loginPage.registerLink", 800);
		clickElement("loginPage.registerLink");
	}

	public void goToResetPasswordPage() {
		Utils.LOG.info("Going to Password Reset page");
		goToLogInPage();
		waitForElementClickable("loginPage.resetPasswordLink", 800);
		clickElement("loginPage.resetPasswordLink");
	}

	// Try resetting password with invalid email
	public void resetPasswordWithInvalidData(String email, String alertMessage, String alertContainerLocator) {
		goToResetPasswordPage();
		waitForElementVisible("forgotPasswordPage.emailInput", 800);
		clearTextInField("forgotPasswordPage.emailInput");
		typeValueInField(email, "forgotPasswordPage.emailInput");
		waitForElementVisible("forgotPasswordPage.resetPasswordButton", 800);
		clickElement("forgotPasswordPage.resetPasswordButton");
		waitForElementVisible(alertContainerLocator, 800);
		assertElementContainsText(alertMessage, alertContainerLocator);
	}

	public void logIn(String usernameLocator, String passwordLocator) {
		goToLogInPage();
		Utils.LOG.info("Logging in");
		waitForElementVisible("loginPage.emailInput", 800);
		typeValueInField(Utils.getConfigPropertyByKey(usernameLocator), "loginPage.emailInput");
		waitForElementVisible("loginPage.passwordInput", 800);
		typeValueInField(Utils.getConfigPropertyByKey(passwordLocator), "loginPage.passwordInput");
		waitForElementVisible("loginPage.logInButton", 800);
		clickElement("loginPage.logInButton");
	}

	public void logInWithIncorrectCredentials(String email, String password, String alertMessage, String alertContainerLocator) {
		goToLogInPage();
		waitForElementVisible("loginPage.emailInput", 800);
		clearTextInField("loginPage.emailInput");
		typeValueInField(email, "loginPage.emailInput");
		waitForElementVisible("loginPage.passwordInput", 800);
		clearTextInField("loginPage.passwordInput");
		typeValueInField(password, "loginPage.passwordInput");
		waitForElementClickable("loginPage.logInButton", 800);
		clickElement("loginPage.logInButton");
		waitForElementVisible(alertContainerLocator, 800);
		assertElementContainsText(alertMessage, alertContainerLocator);
	}

	public void goToProfilePage(String usernameLocator, String passwordLocator) {
		logIn(usernameLocator, passwordLocator);
		waitForElementClickable("homePage.profilePageLink", 800);
		clickElement("homePage.profilePageLink");
	}

	public void logOut() {
		Utils.LOG.info("Logging out");
		waitForElementClickable("homePage.logOutButton", 1000);
		clickElement("homePage.logOutButton");
	}

	public void goToPage(String pageLocator) {
		waitForElementClickable(pageLocator, 800);
		clickElement(pageLocator);
	}

	public void assertElementNotPresentOnLandingPage(String elementLocator) {
		goToLandingPage();
		assertElementNotPresent(elementLocator);
	}

	// Try registering with data missing in a field
	public void registerWithMissingInformation(String validFieldTextOne, String validFieldTextTwo, String fieldLocatorOne, String fieldLocatorTwo) {
		waitForElementVisible(fieldLocatorOne, 800);
		clearTextInField(fieldLocatorOne);
		typeValueInField(validFieldTextOne, fieldLocatorOne);
		waitForElementVisible("registerPage.emailField", 800);
		clearTextInField(fieldLocatorTwo);
		typeValueInField(validFieldTextTwo, fieldLocatorTwo);
		assertElementAttributeContainsText("disabled", "registerPage.registerButton", "class");
	}

	// Try registering with invalid data in one of the fields
	public void registerWithInvalidInformation(String firstName, String lastName, String email) {
		waitForElementVisible("registerPage.firstNameField", 800);
		clearTextInField("registerPage.firstNameField");
		typeValueInField(firstName, "registerPage.firstNameField");
		waitForElementVisible("registerPage.lastNameField", 800);
		clearTextInField("registerPage.lastNameField");
		typeValueInField(lastName, "registerPage.lastNameField");
		waitForElementVisible("registerPage.emailField", 800);
		clearTextInField("registerPage.emailField");
		typeValueInField(email, "registerPage.emailField");
		assertElementAttributeContainsText("disabled", "registerPage.registerButton", "class");
	}

	// Upload an image in a post
	public void uploadPostImage(String locator, String pathToImage) {
		Utils.LOG.info("Uploading image");
		WebElement uploadElement = findElement(Utils.getUIMappingByKey(locator));
		uploadElement.sendKeys(pathToImage);
	}

	// Create a public post with text only
	public void createPublicPostWithText(String postText) {
		Utils.LOG.info("Creating a public post with text in it");
		waitForElementVisible("homePage.newMessageTextarea", 800);
		typeValueInField(postText, "homePage.newMessageTextarea");
		clickElement("homePage.publicRadioButton");
		clickElement("homePage.newMessageShareButton");
	}

	// Create a private post with text only
	public void createPrivatePostWithText(String postText) {
		Utils.LOG.info("Creating a private post");
		waitForElementVisible("homePage.newMessageTextarea", 800);
		typeValueInField(postText, "homePage.newMessageTextarea");
		clickElement("homePage.newMessageShareButton");
	}

	public void assertPostContainsText(String text, String postLocator) {
		waitForElementVisible(postLocator, 1000);
		assertElementContainsText(text, postLocator);
	}

	public void assertPostDoesNotContainText(String text, String postLocator) {
		waitForElementVisible(postLocator, 1000);
		assertElementDoesNotContainText(text, postLocator);
	}

	// Create a comment
	public void createComment(String commentText) {
		Utils.LOG.info("Cerating a comment to a post");
		waitForElementVisible("homePage.latestPostContainer", 800);
		waitForElementClickable("homePage.latestPostCommentButton", 800);
		clickElement("homePage.latestPostCommentButton");
		waitForElementVisible("homePage.latestPostNewCommentTextarea", 800);
		clearTextInField("homePage.latestPostNewCommentTextarea");
		typeValueInField(commentText, "homePage.latestPostNewCommentTextarea");
		clickElement("homePage.latestPostNewCommentShareButton");
	}

	// Delete a comment
	public void deleteAComment() {
		waitForElementVisible("homePage.latestPostLatestFiveCommentsContainer", 800);
		waitForElementClickable("homePage.latestCommentDeleteButton", 800);
		clickElement("homePage.latestCommentDeleteButton");
	}

	// Create a reply to a comment
	public void createReply(String commentText) {
		waitForElementClickable("homePage.latestCommentReplyButton", 800);
		clickElement("homePage.latestCommentReplyButton");
		waitForElementVisible("homePage.latestCommentReplyTextareaContainer", 800);
		clearTextInField("homePage.latestCommentReplyTextarea");
		typeValueInField(commentText, "homePage.latestCommentReplyTextarea");
		clickElement("homePage.latestCommentReplyShareButton");
	}

	// Delete a reply to a comment
	public void deleteReply() {
		waitForElementVisible("homePage.latestPostFirstCommentReply", 800);
		waitForElementClickable("homePage.latestCommentReplyDeleteButton", 800);
		clickElement("homePage.latestCommentReplyDeleteButton");
	}

	// Like a post, comment or reply
	public void likeResource(String likeButtonLocator, String unlikeText) {
		waitForElementClickable(likeButtonLocator, 800);
		clickElement(likeButtonLocator);
		waitForTextToBePresentInElement(unlikeText, likeButtonLocator, 1000);
	}

	// Unlike a comment or reply
	public void unlikeResource(String unLikeButtonLocator, String likeText) {
		waitForElementVisible(unLikeButtonLocator, 800);
		clickElement(unLikeButtonLocator);
		waitForTextToBePresentInElement(likeText, unLikeButtonLocator, 800);
	}

	// Unlike a post
	public void unlikePost(String unLikeButtonLocator, String unlikeText) {
		waitForElementVisible(unLikeButtonLocator, 800);
		clickElement(unLikeButtonLocator);

		try{
			Thread.sleep(2000);
		}catch(Exception e) {
			Utils.LOG.info(e);
		}

	}

	public void goToUserProfile(String currentUserNames, String userLocator) {
		waitForElementVisible("homePage.findMoreBirdWatchersContainer", 800);
		waitForElementClickable(userLocator, 800);
		clickElement(userLocator);
		waitForElementVisible("profilePage.currentUserPersonalInformationContainer", 800);
		String userNames = findElement("profilePage.currentUserFirstName").getText() + " " + findElement("profilePage.currentUserLastName").getText();
		assertStringsAreSame(currentUserNames, userNames);
	}

	public void updateFieldWithoutClickingOnButton(String field, String text) {
		waitForElementVisible(field, 800);
		clearTextInField(field);
		typeValueInField(text, field);
	}

	public void updateField(String field, String text, String button) {
		updateFieldWithoutClickingOnButton(field, text);
		waitForElementClickable(button, 800);
		clickElement(button);
	}

	public void updateProfileFieldInformation(String fieldLocator, String text, String buttonLocator) {
		waitForElementClickable("profilePage.updateProfileButton", 800);
		clickElement("profilePage.updateProfileButton");
		updateField(fieldLocator, text, buttonLocator);
		waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
	}

	public void updatePrivacyLevelOfProfile() {
		waitForElementClickable("homePage.profilePageLink", 800);
		goToPage("homePage.profilePageLink");
		waitForElementClickable("profilePage.userProfilePrivacyButton", 800);
		clickElement("profilePage.userProfilePrivacyButton");
	}

	public void updateProfileFieldWithPresentContainer(String fieldLocator, String text, String buttonLocator) {
		waitForElementClickable("profilePage.updateProfileButton", 800);
		clickElement("profilePage.updateProfileButton");
		updateField(fieldLocator, text, buttonLocator);
		waitForElementVisible("profilePage.updateUserFieldsContainer", 800);
	}

	public void requestFriendship(String currentUserNames, String userLocator, String linkLocator) {
		goToUserProfile(currentUserNames, userLocator);
		waitForElementClickable(linkLocator, 800);
		clickElement(linkLocator);
		assertElementNotPresent(linkLocator);
	}
}
