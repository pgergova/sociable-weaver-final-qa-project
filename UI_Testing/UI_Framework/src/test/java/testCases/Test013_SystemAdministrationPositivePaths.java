package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;

@Feature("System Administration")
public class Test013_SystemAdministrationPositivePaths extends BaseTest {
    private String postText = Utils.getConfigPropertyByKey("postValidText");
    private String commentText = Utils.getConfigPropertyByKey("commentValidText");
    private String replyText = Utils.getConfigPropertyByKey("replyValidText");

    @Test
    @Description("Validate that system admin is able to delete public posts of other users")
    public void OnPresentPublicPostOfAnotherUser_ClickOnDeleteButton_PostGetsDeleted() {
        // Creation of post by regular user
        actions.logIn("user3Email", "user3Password");
        actions.createPublicPostWithText(postText);
        actions.logOut();

        // Assert that system admin is able to delete the post
        actions.logIn("adminEmail", "adminPassword");
        String currentUserNames = actions.findElement("homePage.findMoreBirdWatchersUser3").getText();

        actions.goToUserProfile(currentUserNames, "homePage.findMoreBirdWatchersUser3");
        int userPostsCount = Integer.parseInt(actions.findElement(Utils.getUIMappingByKey("profilePage.userPostsCount")).getText());
        int currentPostCount = userPostsCount;
        actions.waitForElementClickable("profilePage.latestPostDeleteButton", 800);
        actions.clickElement("profilePage.latestPostDeleteButton");
        actions.goToUserProfile(currentUserNames, "homePage.findMoreBirdWatchersUser3");
        userPostsCount = Integer.parseInt(actions.findElement(Utils.getUIMappingByKey("profilePage.userPostsCount")).getText());
        currentPostCount--;
        actions.assertNumbersAreEqual(currentPostCount, userPostsCount);
    }

    @Test
    @Description("Validate that system admin is able to delete private posts of other users")
    public void OnPresentPrivatePostByAnotherUser_ClickOnDeleteButton_PostGetsDeleted() {
        // Creation of post by regular user
        actions.logIn("user3Email", "user3Password");
        actions.createPrivatePostWithText(postText);
        actions.logOut();

        // Assert that system admin is able to delete the post
        actions.logIn("adminEmail", "adminPassword");
        String currentUserNames = actions.findElement("homePage.findMoreBirdWatchersUser3").getText();

        actions.goToUserProfile(currentUserNames, "homePage.findMoreBirdWatchersUser3");
        int userPostsCount = Integer.parseInt(actions.findElement(Utils.getUIMappingByKey("profilePage.userPostsCount")).getText());
        int currentPostCount = userPostsCount;
        actions.waitForElementClickable("profilePage.latestPostDeleteButton", 800);
        actions.clickElement("profilePage.latestPostDeleteButton");
        actions.goToUserProfile(currentUserNames, "homePage.findMoreBirdWatchersUser3");
        userPostsCount = Integer.parseInt(actions.findElement(Utils.getUIMappingByKey("profilePage.userPostsCount")).getText());
        currentPostCount--;
        actions.assertNumbersAreEqual(currentPostCount, userPostsCount);

    }

    @Test
    @Description("Validate that system admin is able to disable and enable other users' accounts")
    public void OnPresentValidAccountOfAnotherUser_ClickEnableDisableButton_AccountStatusChanges() {
        actions.logIn("adminEmail", "adminPassword");

        // Go to Administration page
        actions.waitForElementClickable("homePage.administrationLink", 800);
        actions.clickElement("homePage.administrationLink");
        // Navigate to account of Test User 4
        actions.waitForElementVisible("adminPage.userAdminContainer", 800);
        actions.goToPage("adminPage.navigationSecondPageLink");
        //actions.waitForElementClickable("adminPage.navigationSecondPageLink", 800);
        //actions.clickElement("adminPage.navigationSecondPageLink");
        // Enable account
        actions.waitForElementClickable("adminPage.user4EnableButton", 800);
        actions.clickElement("adminPage.user4EnableButton");
        actions.goToPage("adminPage.navigationSecondPageLink");
        //actions.waitForElementClickable("adminPage.navigationSecondPageLink", 800);
        //actions.clickElement("adminPage.navigationSecondPageLink");
        actions.waitForElementVisible("adminPage.user4Container", 800);
        actions.assertElementPresent("adminPage.user4ViewProfileButton");
        // Disable account
        actions.waitForElementClickable("adminPage.user4DisableButton", 800);
        actions.clickElement("adminPage.user4DisableButton");
        actions.goToPage("adminPage.navigationSecondPageLink");
        //actions.waitForElementClickable("adminPage.navigationSecondPageLink", 800);
        //actions.clickElement("adminPage.navigationSecondPageLink");
        actions.waitForElementVisible("adminPage.user4Container", 800);
        actions.assertElementNotPresent("adminPage.user4ViewProfileButton");
        // Go back to Home page
        actions.clickElement("adminPage.returnToHomePageLink");
    }

    @Test
    @Description("Validate that system admin is able to delete comments of other users")
    public void OnPresentCommentOfAnotherUser_ClickOnDeleteButton_CommentGetsDeleted() {
        // Creation of post by regular user
        int commentsCount = 0;
        actions.logIn("user3Email", "user3Password");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.logOut();
        actions.logIn("adminEmail", "adminPassword");
        String currentUserNames = actions.findElement("homePage.findMoreBirdWatchersUser3").getText();
        actions.goToUserProfile(currentUserNames, "homePage.findMoreBirdWatchersUser3");
        actions.waitForElementClickable("profilePage.latestCommentDeleteButton", 800);
        actions.clickElement("profilePage.latestCommentDeleteButton");
        commentsCount--;
        actions.goToUserProfile(currentUserNames, "homePage.findMoreBirdWatchersUser3");
        actions.waitForElementNotPresent("profilePage.latestPostCommentsContainer", 800);
        actions.assertElementContainsText(commentsCount + " Comments", "profilePage.latestPostCommentsCount");
    }

    @Test
    @Description("Validate that system admin is able to delete replies of other users")
    public void OnPresentCommentReplyOfAnotherUser_ClickOnReplyDeleteButton_ReplyGetsDeleted() {
        int commentsCount = 0;
        actions.logIn("user3Email", "user3Password");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.createReply(replyText);
        commentsCount++;
        actions.logOut();
        actions.logIn("adminEmail", "adminPassword");
        String currentUserNames = actions.findElement("homePage.findMoreBirdWatchersUser3").getText();
        actions.goToUserProfile(currentUserNames, "homePage.findMoreBirdWatchersUser3");
        actions.waitForElementClickable("profilePage.latestReplyDeleteButton", 800);
        actions.clickElement("profilePage.latestReplyDeleteButton");
        commentsCount--;
        actions.goToUserProfile(currentUserNames, "homePage.findMoreBirdWatchersUser3");
        actions.waitForElementNotPresent("profilePage.latestReplyContainer", 800);
        actions.assertElementContainsText(commentsCount + " Comments", "profilePage.latestPostCommentsCount");
    }
}
