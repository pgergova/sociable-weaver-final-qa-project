package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.apache.http.util.Asserts;
import org.junit.Assert;
import org.junit.Test;

@Feature("Feeds")
public class Test009_PublicFeedPositivePaths extends BaseTestWithoutLogOut {
    private String landingPagePublicPostText = Utils.getConfigPropertyByKey("publicPostValidTextOnLandingPage");
    private String landingPagePrivatePostText = Utils.getConfigPropertyByKey("privatePostTextOnLandingPage");
    private String commentText = Utils.getConfigPropertyByKey("commentValidText");

    @Test
    @Description("Validate that unregistered user is able to see a list only with public profiles")
    public void OnNotLoggedIn_SearchForPublicUserProfiles_SeeListOnlyPublicProfilesAreVisible() {
        actions.goToLandingPage();
        actions.logIn("userEmail", "userPassword");
        actions.updatePrivacyLevelOfProfile();
        actions.waitForTextToBePresentInElement("Private", "profilePage.userProfilePrivacyButton", 800);
        actions.logOut();
        actions.goToLandingPage();
        actions.waitForElementVisible("landingPage.usersListContainer", 800);
        actions.assertElementPresent("landingPage.testUserContainer");
        actions.logIn("userEmail", "userPassword");
        actions.updatePrivacyLevelOfProfile();
        actions.waitForTextToBePresentInElement("Public", "profilePage.userProfilePrivacyButton", 800);
        actions.logOut();
        actions.goToLandingPage();
        actions.waitForElementVisible("landingPage.usersListContainer", 800);
        actions.assertElementNotPresent("landingPage.testUserContainer");
    }

    @Test
    @Description("Validate that unregistered user is able to see a list only with public posts")
    public void OnNotLoggedIn_SearchForPublicPosts_SeeListOnlyWithPublicPosts() throws InterruptedException {
        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(landingPagePublicPostText);
        Thread.sleep(1200);
        actions.createPrivatePostWithText(landingPagePrivatePostText);
        Thread.sleep(1000);
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.assertElementContainsText(landingPagePrivatePostText, "homePage.latestPostText");
        actions.logOut();
        actions.goToLandingPage();
        actions.waitForElementVisible("landingPage.postsContainer", 800);
        actions.assertElementContainsText(landingPagePublicPostText, "landingPage.latestPostContainer");
        actions.assertElementDoesNotContainText(landingPagePrivatePostText, "landingPage.latestPostContainer");
    }

    @Test
    @Description("Validate that unregistered user is able to see a list with popular posts")
    public void OnNotLoggedIn_SearchForListWithPopularPosts_SeeListWithPopularPosts(){
        actions.goToLandingPage();
        actions.waitForElementVisible("landingPage.HeadListWithPopularPosts",800);
        actions.assertElementPresent("landingPage.listWithPopularPosts");
    }

    @Test
    @Description("Validate that unregistered user can see number of comments of any public post")
    public void OnLoggedInUser_TakeALookAtLatestPost_SeeNumberOfComments(){
        int commentsCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(landingPagePublicPostText);
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.assertElementContainsText(landingPagePublicPostText, "homePage.latestPostText");
        actions.createComment(commentText);
        commentsCount++;
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.logOut();
        actions.goToLandingPage();
        actions.waitForElementVisible("landingPage.HeadListWithPopularPosts",800);
        actions.assertElementPresent("landingPage.numberOfComments");
        actions.assertNumbersAreEqual(commentsCount, Integer.parseInt(actions.findElement("landingPage.numberOfComments").getText()));
    }
}
