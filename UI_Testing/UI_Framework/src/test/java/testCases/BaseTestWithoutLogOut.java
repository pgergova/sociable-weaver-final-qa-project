package testCases;

import com.telerikacademy.uiframework.UserActions;
import com.telerikacademy.uiframework.Utils;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BaseTestWithoutLogOut {
    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp(){
        UserActions.loadBrowser();
    }

    @AfterClass
    public static void tearDown(){
        UserActions.quitDriver();
    }
}

