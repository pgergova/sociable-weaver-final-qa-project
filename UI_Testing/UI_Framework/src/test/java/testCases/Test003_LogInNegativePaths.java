package testCases;

import com.telerikacademy.uiframework.UserActions;
import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.junit.*;

@Feature("LogIn")
public class Test003_LogInNegativePaths extends BaseTestWithoutLogOut {
    private String validEmail = Utils.getConfigPropertyByKey("adminEmail");
    private String validPassword = Utils.getConfigPropertyByKey("adminPassword");
    private String incorrectEmail = Utils.getConfigPropertyByKey("incorrectEmail");
    private String incorrectPassword = Utils.getConfigPropertyByKey("incorrectPassword");
    private String disabledEmail = Utils.getConfigPropertyByKey("disabledEmailAddress");
    private String badCredentialsAlertMessage = Utils.getConfigPropertyByKey("badCredentialsAlertMessage");
    private String disabledAccountAlertMessage = Utils.getConfigPropertyByKey("loginPageDisabledAccountAlertMessage");


    @Test
    @Description("User is unable to log in with incorrect email")
    public void OnIncorrectEmail_ClickLogInButton_IncorrectUsernameOrPasswordAlert() {
        actions.logInWithIncorrectCredentials(incorrectEmail, validPassword, badCredentialsAlertMessage, "loginPage.alertContainer");
    }

    @Test
    @Description("User is unable to log in with incorrect password")
    public void OnIncorrectPassword_ClickLogInButton_IncorrectUsernameOrPasswordAlert() {
        actions.logInWithIncorrectCredentials(validEmail, incorrectPassword, badCredentialsAlertMessage, "loginPage.alertContainer");
    }

    @Test
    @Description("Validate user is unable to log in with by leaving email field blank.")
    public void OnEmptyEmailField_ClickLogInButton_IncorrectUsernameOrPasswordAlert() {
        actions.logInWithIncorrectCredentials("", validPassword, badCredentialsAlertMessage, "loginPage.alertContainer");
    }

    @Test
    @Description("Validate user is unable to log in with by leaving password blank.")
    public void OnEmptyPasswordField_ClickLogInButton_IncorrectUsernameOrPasswordAlert() {
        actions.logInWithIncorrectCredentials(validEmail, "", badCredentialsAlertMessage, "loginPage.alertContainer");
    }

    @Test
    @Description("Validate user is unable to log in with by leaving email and password fields blank.")
    public void OnEmptyPasswordAndEmailFields_ClickLogIn_IncorrectUsernameOrPasswordAlert() {
        actions.logInWithIncorrectCredentials("", "", badCredentialsAlertMessage, "loginPage.alertContainer");
    }

    @Test
    @Description("Validate user is unable to log into a disabled account.")
    public void OnDisabledEmailAddress_ClickOnLogInButton_AlertIsReceived() {
        actions.logInWithIncorrectCredentials(disabledEmail, validPassword, disabledAccountAlertMessage, "loginPage.disabledAccountAlertContainer");
    }
}