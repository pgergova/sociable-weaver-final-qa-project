package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;

@Feature("LogIn")
public class Test002_LogInPositivePaths extends BaseTestWithoutLogOut {

    @Test
    @Description("Validate that user is able to log in with valid credentials")
    public void OnValidEmailAndPassword_ClickLogIn_UserIsRedirectedToHomePage() {
        actions.logIn("adminEmail", "adminPassword");
        actions.logOut();
        actions.waitForElementVisible("logoutPage.alertContainer", 800);
        actions.assertElementContainsText(Utils.getConfigPropertyByKey("loggedOutAlertMessage"), "logoutPage.alertContainer");
    }
}
