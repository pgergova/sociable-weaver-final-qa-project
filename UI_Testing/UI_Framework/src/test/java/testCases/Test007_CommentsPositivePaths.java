package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;

@Feature("Comments")
public class Test007_CommentsPositivePaths extends BaseTest {
    private String postText = Utils.getConfigPropertyByKey("postValidText");
    private String commentText = Utils.getConfigPropertyByKey("commentValidText");
    private String replyText = Utils.getConfigPropertyByKey("replyValidText");
    private String likeText = " Like";
    private String unlikeText = "UnLike";

    @Test
    @Description("Validate that user is able to create and delete their own comment on their own post")
    public void OnPresentOwnPost_ClickOnCommentButton_AndEnterValidCommentText_CommentIsCreated() {
        int commentsCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.assertElementContainsText(commentText, "homePage.latestPostFirstComment");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.deleteAComment();
        commentsCount--;
        actions.waitForElementNotPresent("homePage.latestPostLatestFiveCommentsContainer", 800);
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
    }

    @Test
    @Description("Validate that user is able to create and delete their own comment on posts of other users")
    public void OnPresentPostOfAnotherUser_ClickOnCommentButton_AndEnterValidCommentText_CommentIsCreated() {
        int commentsCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.logOut();
        actions.logIn("user3Email", "user3Password");
        actions.createComment(commentText);
        commentsCount++;
        actions.assertElementContainsText(commentText, "homePage.latestPostFirstComment");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.deleteAComment();
        commentsCount--;
        actions.waitForElementNotPresent("homePage.latestPostLatestFiveCommentsContainer", 800);
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
    }

    @Test
    @Description("Validate user is able to create and delete their own reply to their own comment")
    public void OnPresentOwnComment_ClickOnReplyButton_AndEnterValidReplyText_CreateAndDeleteAReply() {
        int commentsCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.createReply(replyText);
        commentsCount++;
        actions.assertElementContainsText(replyText, "homePage.latestPostFirstCommentReply");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.deleteReply();
        commentsCount--;
        actions.waitForElementNotPresent("homePage.latestPostFirstCommentReply", 800);
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
    }

    @Test
    @Description("Validate user is able to create and delete own replies to other users' comments")
    public void OnPresentOtherUsersComment_ClickOnReplyButton_AndEnterValidReplyText_CreateAndDeleteAReply() {
        int commentsCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.logOut();
        actions.logIn("user3Email", "user3Password");
        actions.createReply(replyText);
        commentsCount++;
        actions.assertElementContainsText(replyText, "homePage.latestPostFirstCommentReply");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.deleteReply();
        commentsCount--;
        actions.waitForElementNotPresent("homePage.latestPostFirstCommentReply", 800);
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
    }

    @Test
    @Description("Validate that user is able to like and unlike their own comments")
    public void OnPresentOwnComments_ClickOnTheCommentLikeButton_LikeCountsChange() {
        int commentsCount = 0;
        int commentLikesCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.assertElementContainsText(commentText, "homePage.latestPostFirstComment");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.likeResource("homePage.latestCommentLikeButton", unlikeText);
        commentLikesCount++;
        actions.assertElementContainsText(commentLikesCount + " UnLike", "homePage.latestCommentLikeButton");
        actions.unlikeResource("homePage.latestCommentLikeButton", likeText);
        commentLikesCount--;
        actions.assertElementContainsText(commentLikesCount + " Like", "homePage.latestCommentLikeButton");
    }

    @Test
    @Description("Validate that user is able to like and unlike other users' comments")
    public void OnPresentOtherUsersComments_ClickOnTheCommentLikeButton_LikeCountsChange() {
        int commentsCount = 0;
        int commentLikesCount = 0;

        actions.logIn("user3Email", "user3Password");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.assertElementContainsText(commentText, "homePage.latestPostFirstComment");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.likeResource("homePage.latestCommentLikeButton", unlikeText);
        commentLikesCount++;
        actions.assertElementContainsText(commentLikesCount + " UnLike", "homePage.latestCommentLikeButton");
        actions.unlikeResource("homePage.latestCommentLikeButton", likeText);
        commentLikesCount--;
        actions.assertElementContainsText(commentLikesCount + " Like", "homePage.latestCommentLikeButton");
    }

    @Test
    @Description("Validate that user is able to like and unlike their own replies")
    public void OnPresentOwnCommentReplies_ClickOnLikeButton_LikeCountChanges() {
        int commentsCount = 0;
        int replyLikesCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.assertElementContainsText(commentText, "homePage.latestPostFirstComment");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.createReply(replyText);
        commentsCount++;
        actions.assertElementContainsText(replyText, "homePage.latestPostFirstCommentReply");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.likeResource("homePage.latestCommentReplyLikeButton", unlikeText);
        replyLikesCount++;
        actions.assertElementContainsText(replyLikesCount + " UnLike", "homePage.latestCommentReplyLikeButton");
        actions.unlikeResource("homePage.latestCommentReplyLikeButton", likeText);
        replyLikesCount--;
        actions.assertElementContainsText(replyLikesCount + " Like", "homePage.latestCommentReplyLikeButton");
    }

    @Test
    @Description("Validate that user is able to like and unlike replies of other users")
    public void OnPresentRepliesByOtherUsers_ClickOnLikeButton_LikeCountChanges() {
        int commentsCount = 0;
        int replyLikesCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.assertElementContainsText(commentText, "homePage.latestPostFirstComment");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.createReply(replyText);
        commentsCount++;
        actions.assertElementContainsText(replyText, "homePage.latestPostFirstCommentReply");
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.logOut();
        actions.logIn("user3Email", "user3Password");
        actions.likeResource("homePage.latestCommentReplyLikeButton", unlikeText);
        replyLikesCount++;
        actions.assertElementContainsText(replyLikesCount + " UnLike", "homePage.latestCommentReplyLikeButton");
        actions.unlikeResource("homePage.latestCommentReplyLikeButton", likeText);
        replyLikesCount--;
        actions.assertElementContainsText(replyLikesCount + " Like", "homePage.latestCommentReplyLikeButton");
    }


    @Test
    @Description("Validate that user is able to see n most recent post comments")
    public void OnNoMoreThanFiveCommentToAPost_TakeALookAtComments_NoAllCommentsLinkIsPresent() throws InterruptedException {
        int commentsCount  = 0;
        String lastCommentText="";

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        for (int i = 1; i <= 5; i++) {
            lastCommentText = commentText + " " + i;
            actions.createComment(lastCommentText);
            commentsCount++;
            Thread.sleep(1000);
        }
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.waitForElementNotPresent("homePage.latestPostAllCommentsLink", 1000);
        actions.assertElementContainsText(lastCommentText, "homePage.latestPostLatestFiveCommentsFifthComment");
    }

    @Test
    @Description("Validate that user is able to see older comments")
    public void OnMoreThan5CommentsPresent_ClickOnAllCommentsLink_MoreCommentsAreDisplayed() throws InterruptedException {
        int commentsCount  = 0;
        String lastCommentText="";

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        for (int i = 1; i <= 7; i++) {
            lastCommentText = commentText + " " + i;
            actions.createComment(lastCommentText);
            commentsCount++;
            Thread.sleep(1000);
        }

        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.waitForElementClickable("homePage.latestPostAllCommentsLink", 1000);
        actions.clickElement("homePage.latestPostAllCommentsLink");
        actions.assertElementContainsText(lastCommentText, "homePage.latestPostLastCommentText");
    }
}
