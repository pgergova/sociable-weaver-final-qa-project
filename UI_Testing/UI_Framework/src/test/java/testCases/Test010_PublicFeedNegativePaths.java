package testCases;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;

@Feature("Feeds")
public class Test010_PublicFeedNegativePaths extends BaseTestWithoutLogOut {

    @Test
    @Description("Validate that unregistered user is unable to create posts")
    public void OnNotLoggedIn_SearchForNewPostTextarea_NoTextareaForNewPostIsPresent() {
        actions.assertElementNotPresentOnLandingPage("homePage.newMessageTextarea");
    }

    @Test
    @Description("Validate that unregistered user is unable to delete posts")
    public void OnNotLoggedIn_SearchForDeleteButton_NoDeleteButtonIsPresent() {
        actions.assertElementNotPresentOnLandingPage("homePage.latestPostDeleteButton");
    }

    @Test
    @Description("Validate that unregistered user is unable to like a post")
    public void OnNotLoggedIn_SearchForLikeButton_NoLikeButtonIsPresent() {
        actions.assertElementNotPresentOnLandingPage("homePage.latestPostLikeButton");
    }

    @Test
    @Description("Validate that unregistered user is unable to post comments")
    public void OnNotLoggedIn_SearchForCommentButton_NoCommentButtonIsPresent() {
        actions.assertElementNotPresentOnLandingPage("homePage.latestPostCommentButton");
    }

    @Test
    @Description("Validate that unregistered user is unable to like comments")
    public void OnNotLoggedIn_SearchForCommentLikeButton_NoCommentLikeButtonIsPresent() {
        actions.assertElementNotPresentOnLandingPage("homePage.latestCommentLikeButton");
    }

    @Test
    @Description("Validate that unregistered user is unable to reply to comments")
    public void OnNotLoggedIn_SearchForCommentReplyArea_NoCommentReplyTextareaIsPresent() {
        actions.assertElementNotPresentOnLandingPage("homePage.latestCommentReplyShareButton");
    }

    @Test
    @Description("Validate that unregistered user is unable to like replies")
    public void OnNotLoggedIn_SearchForCommentReplyLikeButton_NoCommentReplyLikeButtonIsPresent() {
        actions.assertElementNotPresentOnLandingPage("homePage.latestCommentReplyLikeButton");
    }
}
