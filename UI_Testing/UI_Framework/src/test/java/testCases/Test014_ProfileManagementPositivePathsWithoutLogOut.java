package testCases;

import io.qameta.allure.Description;
import org.junit.Test;

public class Test014_ProfileManagementPositivePathsWithoutLogOut extends BaseTestWithoutLogOut {
    @Test
    // Fails due to bug #53
    @Description("Validate that user is able to change privacy of profile")
    public void OnPrivateProfile_ClickOnVisibilityButton_VisibilityChangesToPrivate() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.waitForElementClickable("profilePage.userProfilePrivacyButton", 800);
        actions.assertElementContainsText("Public", "profilePage.userProfilePrivacyButton");
        actions.clickElement("profilePage.userProfilePrivacyButton");
        actions.waitForTextToBePresentInElement("Private", "profilePage.userProfilePrivacyButton", 800);
        actions.logOut();
        actions.goToLandingPage();
        actions.waitForElementVisible("landingPage.usersListContainer", 800);
        actions.assertElementPresent("landingPage.testUserContainer");
        actions.goToProfilePage("userEmail", "userPassword");
        actions.waitForElementClickable("profilePage.userProfilePrivacyButton", 800);
        actions.assertElementContainsText("Private", "profilePage.userProfilePrivacyButton");
        actions.clickElement("profilePage.userProfilePrivacyButton");
        actions.waitForTextToBePresentInElement("Public", "profilePage.userProfilePrivacyButton", 800);
        actions.logOut();
        actions.goToLandingPage();
        actions.waitForElementVisible("landingPage.usersListContainer", 800);
        actions.assertElementNotPresent("landingPage.testUserContainer");
    }
}
