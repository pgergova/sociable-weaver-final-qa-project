package testCases;

import com.sun.corba.se.impl.logging.UtilSystemException;
import com.telerikacademy.uiframework.UserActions;
import com.telerikacademy.uiframework.Utils;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest extends BaseTestWithoutLogOut{
	UserActions actions = new UserActions();

	@After
	public void afterTest() {
		actions.logOut();
	}

}
