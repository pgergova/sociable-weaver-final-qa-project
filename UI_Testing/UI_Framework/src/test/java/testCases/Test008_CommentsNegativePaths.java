package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

@Feature("Comments")
public class Test008_CommentsNegativePaths extends BaseTest {
    private String postText = Utils.getConfigPropertyByKey("postValidText");
    private String tooLongCommentText = Utils.getConfigPropertyByKey("tooLongText");
    private String croppedCommentText = Utils.getConfigPropertyByKey("croppedText");
    private String commentText = Utils.getConfigPropertyByKey("commentValidText");
    private String replyText = Utils.getConfigPropertyByKey("replyValidText");
    private String emptyTextAlertMessage = Utils.getConfigPropertyByKey("missingTextAlertMessage");

    @Test
    @Description("Validate that user is unable to enter too long comment to a post")
    public void OnTooLongCommentText_ClickOnShareButton_CommentGetsCroppedToTheLimit() {
        String message;
        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment(tooLongCommentText);
        actions.waitForElementVisible("homePage.latestPostFirstComment", 800);
        String currentCommentText = actions.findElement("homePage.latestPostFirstComment").getText();
        actions.assertStringsAreSame(croppedCommentText, currentCommentText);
    }

    @Test
    @Description("Validate that user is unable to create a blank comment")
    public void OnBlankCommentText_ClickOnShareButton_NoCommentIsCreated() {
        String message;
        int commentsCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment("");
        message = actions.findElement("homePage.latestPostNewCommentTextarea").getAttribute("validationMessage");
        actions.assertStringsAreSame(emptyTextAlertMessage, message);
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
    }


    @Test
    @Description("Validate that user is unable to create a blank reply")
    public void OnBlankReplyText_ClickOnShareButton_AlertMessageIsDisplayed() {
        String message;
        int commentsCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.createReply("");
        message = actions.findElement("homePage.latestPostNewCommentTextarea").getAttribute("validationMessage");
        actions.assertStringsAreSame(emptyTextAlertMessage, message);
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
    }

    @Test
    @Description("Validate that user is unable to create a reply with too long text")
    public void OnTooLongReplyText_ClickOnShareButton_ReplyGetsCroppedToTheLimit() {int commentsCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.createReply(tooLongCommentText);
        commentsCount++;
        actions.waitForElementVisible("homePage.latestPostFirstCommentReply", 800);
        String currentReplyText = actions.findElement("homePage.latestPostFirstCommentReply").getText();
        actions.assertStringsAreSame(croppedCommentText, currentReplyText);
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
    }

    @Test
    @Description("Validate that user is unable to delete other users' comments")
    public void OnPresentCommentsByOtherUsers_SearchForCommentDeleteButton_NoDeleteButtonIsPresent() {
        int commentsCount = 0;

        actions.logIn("user3Email", "user3Password");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.waitForElementVisible("homePage.latestPostFirstComment", 800);
        actions.assertElementNotPresent("homePage.latestCommentDeleteButton");
    }

    @Test
    @Description("Validate that user is unable to delete other users' comments")
    public void OnPresentRepliesByOtherUsers_SearchForReplyDeleteButton_NoDeleteButtonIsPresent() {
        int commentsCount = 0;

        actions.logIn("user3Email", "user3Password");
        actions.createPublicPostWithText(postText);
        actions.createComment(commentText);
        commentsCount++;
        actions.createReply(replyText);
        commentsCount++;
        actions.assertElementContainsText(commentsCount + " Comments", "homePage.latestPostCommentsCount");
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.waitForElementVisible("homePage.latestPostFirstCommentReply", 800);
        actions.assertElementNotPresent("homePage.latestCommentReplyDeleteButton");
    }
}
