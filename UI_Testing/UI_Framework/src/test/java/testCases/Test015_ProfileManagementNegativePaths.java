package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Ignore;
import org.junit.Test;

@Feature("Profile Management")
public class Test015_ProfileManagementNegativePaths extends BaseTestWithoutLogOut {
    private String tooLongFirstName = Utils.getConfigPropertyByKey("tooLongFirstName");
    private String croppedFirstName = Utils.getConfigPropertyByKey("croppedFirstName");
    private String tooLongLastName = Utils.getConfigPropertyByKey("tooLongLastName");
    private String croppedLastName = Utils.getConfigPropertyByKey("croppedLastName");
    private String tooLongNationality = Utils.getConfigPropertyByKey("tooLongNationality");
    private String croppedNationality = Utils.getConfigPropertyByKey("croppedNationality");
    private String tooLongCity=Utils.getConfigPropertyByKey("tooLongCIty");
    private String croppedCity = Utils.getConfigPropertyByKey("croppedCity");
    private String tooShortNationality= Utils.getConfigPropertyByKey("tooShortNationality");
    private String tooShortCity= Utils.getConfigPropertyByKey("tooShortCity");
    private String alertForMissingInput = Utils.getConfigPropertyByKey("missingTextAlertMessage");
    private String alertForTooShortInput= Utils.getConfigPropertyByKey("lessThanThreeCharactersAlertMessage");
    private String validFirstName = Utils.getConfigPropertyByKey("validFirstName");
    private String validLastName = Utils.getConfigPropertyByKey("validLastName");

    public void updateProfileFieldInformation(String fieldLocator, String text, String buttonLocator) {
        actions.waitForElementClickable("profilePage.updateProfileButton", 800);
        actions.clickElement("profilePage.updateProfileButton");
        actions.updateField(fieldLocator, text, buttonLocator);
    }

    @Test
    @Description("Validate that user is unable to enter too long first name - above 20 characters")
    public void OnTooLongFirstName_ClickOnUpdateButton_AlertForTooLongFirstNameIsDisplayed() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldInformation("profilePage.firstNameInputField", tooLongFirstName, "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        String currentFirstName = actions.findElement("profilePage.currentUserFirstName").getText();
        actions.assertStringsAreSame(croppedFirstName, currentFirstName);
        actions.updateProfileFieldInformation("profilePage.firstNameInputField", validFirstName, "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        currentFirstName = actions.findElement("profilePage.currentUserFirstName").getText();
        actions.assertStringsAreSame(validFirstName, currentFirstName);
    }

    @Test
    @Description("Validate that user is unable to enter too long last name - above 20 characters")
    public void OnTooLongLastName_ClickOnUpdateButton_AlertForTooLongLastNameIsDisplayed() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldInformation("profilePage.lastNameInputField", tooLongLastName, "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        String currentLastName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserLastName")).getText();
        actions.assertStringsAreSame(currentLastName, croppedLastName);
        actions.updateProfileFieldInformation("profilePage.lastNameInputField", validLastName, "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        currentLastName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserLastName")).getText();
        actions.assertStringsAreSame(currentLastName, validLastName);
    }

    @Test
    @Description("Validate that user is unable to enter too long nationality - above 50 characters")
    public void OnTooLongNationality_ClickOnUpdateButton_AlertForTooLongNationalityIsDisplayed() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldInformation("profilePage.nationalityInputField", tooLongNationality, "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        String currentNationality = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserNationality")).getText();
        actions.assertStringsAreSame(currentNationality, croppedNationality);
        actions.updateProfileFieldInformation("profilePage.nationalityInputField", "", "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        currentNationality = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserNationality")).getText();
        actions.assertStringsAreSame(currentNationality, "");
    }

    @Test
    @Description("Validate that user is unable to enter too long city - above 50 characters")
    public void OnTooLongCity_ClickOnUpdateButton_AlertForTooLongCityIsDisplayed() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldInformation("profilePage.cityInputField", tooLongCity, "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        String currentCity = actions.findElement("profilePage.currentUserCity").getText();
        actions.assertStringsAreSame(currentCity, croppedCity);
        actions.updateProfileFieldInformation("profilePage.cityInputField", "", "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        currentCity = actions.findElement("profilePage.currentUserCity").getText();
        actions.assertStringsAreSame(currentCity, "");
    }

    @Test
    @Description("Validate that user is unable to enter blank first name in profile")
    public void OnBlankFirstName_ClickOnUpdateButton_AlertForBlankFieldIsDisplayed() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldWithPresentContainer("profilePage.firstNameInputField", "", "profilePage.submitFormButton");
        String message = actions.findElement("profilePage.firstNameInputField").getAttribute("validationMessage");
        actions.assertStringsAreSame(alertForMissingInput, message);
        actions.updateField("profilePage.firstNameInputField", validFirstName, "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        String currentFirstName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserFirstName")).getText();
        actions.assertStringsAreSame(currentFirstName, validFirstName);
    }

    @Test
    @Description("Validate that user is unable to enter blank last name in profile")
    public void OnBlankLastName_ClickOnUpdateButton_AlertForBlankFieldIsDisplayed() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldWithPresentContainer("profilePage.lastNameInputField", "", "profilePage.submitFormButton");
        String message = actions.findElement("profilePage.lastNameInputField").getAttribute("validationMessage");
        actions.assertStringsAreSame(alertForMissingInput, message);
        actions.updateField("profilePage.lastNameInputField", validLastName, "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        String currentLastName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserLastName")).getText();
        actions.assertStringsAreSame(currentLastName, validLastName);
    }

    @Test
    @Description("Validate that user is unable to enter nationality between 1 and 3 characters")
    public void OnNationalityOfLessThanThreeChars_ClickOnUpdateButton_AlertIsDisplayed() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldWithPresentContainer("profilePage.nationalityInputField", tooShortNationality, "profilePage.submitFormButton");
        String message = actions.findElement("profilePage.nationalityInputField").getAttribute("validationMessage");
        actions.assertStringsAreSame(alertForTooShortInput, message);
        actions.updateField("profilePage.nationalityInputField", "", "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        actions.assertElementIsNotVisible("profilePage.currentUserNationality");
    }

    @Test
    @Description("Validate that user is unable to enter city between 1 and 3 characters")
    public void OnCityOfLessThanThreeChars_ClickOnUpdateButton_AlertIsDisplayed() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldWithPresentContainer("profilePage.cityInputField", tooShortCity, "profilePage.submitFormButton");
        String message = actions.findElement("profilePage.cityInputField").getAttribute("validationMessage");
        System.out.println(message);
        actions.assertStringsAreSame(alertForTooShortInput, message);
        actions.updateField("profilePage.cityInputField", "", "profilePage.submitFormButton");
        actions.waitForElementNotPresent("profilePage.updateUserFieldsContainer", 800);
        actions.assertElementIsNotVisible("profilePage.currentUserCity");
    }
}
