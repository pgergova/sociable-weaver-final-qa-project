package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

@Feature("Feeds")
public class Test012_PrivateFeedsNegativePaths extends BaseTest {
    private String publicPostText = Utils.getConfigPropertyByKey("publicPostValidText");
    private String privatePostText = Utils.getConfigPropertyByKey("privatePostValidText");

    @Test
    @Description("Validate user is unable to see private posts of non-friends in personal feed")
    public void OnPresentPrivatePostsOfNonFriends_GoToHomePage_NoPrivatePostsOfNonFriendsArePresent() throws InterruptedException {
        actions.logIn("user5Email", "user5Password");
        String userNames = actions.findElement("homePage.currentUserNamesContainer").getText();
        String privateTestFromUser5 = "Test private post from user" + userNames;
        actions.createPrivatePostWithText(privateTestFromUser5);
        actions.assertPostContainsText(privateTestFromUser5, "homePage.latestPostText");
        Thread.sleep(1000);
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.assertPostDoesNotContainText(privateTestFromUser5, "homePage.latestPostText");
    }

    @Test
    @Description("Validate user is unable to see public posts of non-friends in personal feed")
    // Fails due to bug #54
    public void OnPresentPublicPostsOfNonFriend_GoToHomePage_NoPublicPostsOfNonFriendIsPresent() throws InterruptedException {
        actions.logIn("user5Email", "user5Password");
        String userNames = actions.findElement("homePage.currentUserNamesContainer").getText();
        String publicPostTextFromUser5 = "Test public post from user" + userNames;
        actions.createPublicPostWithText(publicPostTextFromUser5);
        actions.assertPostContainsText(publicPostTextFromUser5, "homePage.latestPostText");
        Thread.sleep(1000);
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.assertPostDoesNotContainText(publicPostTextFromUser5, "homePage.latestPostText");
    }

    @Test
    @Description("Validate user is unable to see private posts in non-friend's private profile feed")
    public void OnPresentPrivatePostsOfNonFriend_GoToNonFriendProfilePage_NoPrivatePostIsVisible() {
        actions.logIn("user5Email", "user5Password");
        actions.createPrivatePostWithText(privatePostText);
        actions.assertPostContainsText(privatePostText, "homePage.latestPostText");
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.goToPage("homePage.findMoreBirdWatchersUser5");
        String currentUserLastName = actions.findElement("profilePage.currentUserLastName").getText();
        actions.assertStringsAreSame("User 5", currentUserLastName);
        actions.assertElementDoesNotContainText(privatePostText, "profilePage.latestPostInFeedTextContainer");
    }


    @Test
    @Description("Validate user is unable to see private post in non-friend's public profile feed")
    public void OnPresentPrivatePostOfNonFriend_GoToPublicProfileOfNonFriend_CannotSeePrivatePost() {
        actions.logIn("user6Email", "user6Password");
        actions.createPrivatePostWithText(privatePostText);
        actions.assertPostContainsText(privatePostText, "homePage.latestPostText");
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.goToPage("homePage.findMoreBirdWatchersUser6");
        String currentUserLastName = actions.findElement("profilePage.currentUserLastName").getText();
        actions.assertStringsAreSame("User 6", currentUserLastName);
        if(actions.getSizeOfElements("profilePage.postContainer") > 1) {
            actions.assertElementDoesNotContainText(privatePostText, "profilePage.latestPostInFeedTextContainer");
        }
    }
}
