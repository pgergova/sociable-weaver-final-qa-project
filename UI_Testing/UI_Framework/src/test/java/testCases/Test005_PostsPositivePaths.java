package testCases;

import com.telerikacademy.uiframework.UserActions;
import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.*;
import org.openqa.selenium.WebElement;

@Feature("Posts")
public class Test005_PostsPositivePaths extends BaseTest {
    private String postText = Utils.getConfigPropertyByKey("postValidText");
    private String postUpdatedText = Utils.getConfigPropertyByKey("postUpdatedText");
    private String pathToImage = System.getProperty("user.dir") + "\\src\\test\\resources\\external\\sociable_weaver.jpg";
    private String postTextForDeletion = Utils.getUIMappingByKey("deletedPostValidText");
    private String unlikeText = "UnLike";

    @Test
    @Description("Validate that user is able to create a private post with text")
    public void OnValidText_ClickOnShareButton_PrivatePostIsCreated() {
        actions.logIn("userEmail", "userPassword");

        int userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        int newPostCount = userPostsCount;

        actions.createPrivatePostWithText(postText);
        newPostCount++;
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.assertElementContainsText(postText,"homePage.latestPostText");
        actions.assertElementContainsText("Public", "homePage.latestPostPrivacyButton");
        userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        actions.assertNumbersAreEqual(newPostCount, userPostsCount);
    }


    @Test
    @Description("Validate user is able to create a public post with text")
    public void OnValidText_ClickOnShareButton_PublicPostIsCreated() {
        actions.logIn("userEmail", "userPassword");

        int userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        int newPostCount = userPostsCount;

        actions.createPublicPostWithText(postText);
        newPostCount++;
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.assertElementContainsText(postText,"homePage.latestPostText");
        actions.assertElementContainsText("Private", "homePage.latestPostPrivacyButton");
        userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        actions.assertNumbersAreEqual(newPostCount, userPostsCount);
    }


    @Test
    @Description("Validate that user is able to create a private post with text and image")
    public void OnValidTextAndImage_ClickOnShareButton_PrivatePostIsCreated() {
        actions.logIn("adminEmail", "adminPassword");

        int userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        int newPostCount = userPostsCount;

        actions.waitForElementVisible("homePage.newMessageTextarea", 800);
        actions.typeValueInField(postText, "homePage.newMessageTextarea");
        actions.clickElement("homePage.imagesTab");
        actions.uploadPostImage("homePage.browseImageButton", pathToImage);
        actions.clickElement("homePage.newMessageShareButton");
        newPostCount++;
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.assertElementContainsText(postText,"homePage.latestPostText");
        actions.assertElementContainsText("Public", "homePage.latestPostPrivacyButton");
        userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        actions.assertNumbersAreEqual(newPostCount, userPostsCount);
    }


    @Test
    @Description("Validate that user is able to create a public post with text and image")
    public void OnValidTextAndImage_ClickOnShareButton_PublicPostIsCreated() {
        actions.logIn("adminEmail", "adminPassword");

        int userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        int newPostCount = userPostsCount;

        actions.waitForElementVisible("homePage.newMessageTextarea", 800);
        actions.typeValueInField(postText, "homePage.newMessageTextarea");
        actions.clickElement("homePage.imagesTab");
        actions.uploadPostImage("homePage.browseImageButton", pathToImage);
        actions.clickElement("homePage.publicRadioButton");
        actions.clickElement("homePage.newMessageShareButton");
        newPostCount++;
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.assertElementContainsText(postText, "homePage.latestPostText");
        actions.assertElementContainsText("Private", "homePage.latestPostPrivacyButton");
        userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        actions.assertNumbersAreEqual(newPostCount, userPostsCount);
    }


    @Test
    @Description("Validate that user is able to update their own post")
    public void OnValidUpdatedText_ClickOnUpdatePostButton_PostIsUpdated() {
        actions.logIn("userEmail", "userPassword");

        int userPostsCount;
        int currentUserPostsCount;

        actions.createPublicPostWithText(postText);
        userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.clickElement("homePage.latestPostUpdateButton");
        actions.waitForElementVisible("homePage.latestPostUpdatePostTextarea", 800);
        actions.clearTextInField("homePage.latestPostUpdatePostTextarea");
        actions.typeValueInField(postUpdatedText, "homePage.latestPostUpdatePostTextarea");
        actions.clickElement("homePage.latestPostUpdatePostButton");
        actions.assertElementContainsText(postUpdatedText,"homePage.latestPostText");
        currentUserPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        actions.assertNumbersAreEqual(userPostsCount, currentUserPostsCount);
    }

    @Test
    @Description("Validate that user is able to delete their own post")
    public void OnLocatedOwnPost_ClickDeleteButton_PostGetsDeleted() {
        actions.logIn("userEmail", "userPassword");

        int userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());;
        int currentUserPostsCount = userPostsCount;

        actions.createPublicPostWithText(postTextForDeletion);
        currentUserPostsCount++;
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.waitForElementVisible("homePage.latestPostDeleteButton", 800);
        actions.clickElement("homePage.latestPostDeleteButton");
        currentUserPostsCount--;
        actions.assertElementDoesNotContainText(postTextForDeletion, "homePage.latestPostContainer");
        actions.assertNumbersAreEqual(currentUserPostsCount, userPostsCount);
    }

    @Test
    @Description("Validate that user is able to change visibility of a post")
    public void OnPresentPostVisibilityIcon_ClickOnVisibilityButton_VisibilityChanges() {
        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.waitForElementVisible("homePage.latestPostPrivacyButton", 800);
        actions.assertElementContainsText("Private", "homePage.latestPostPrivacyButton");
        actions.clickElement("homePage.latestPostPrivacyButton");
        actions.waitForTextToBePresentInElement("Public","homePage.latestPostPrivacyButton", 800 );
        actions.assertElementContainsText("Public", "homePage.latestPostPrivacyButton");
    }

    @Test
    @Description("Validate that user is able to like and unlike their own post")
    public void OnPresentOwnPost_ClickOnPostLikeButton_LikeCountChanges() {
        int likesCount = 0;
        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.likeResource("homePage.latestPostLikeButton", unlikeText);
        likesCount++;
        actions.assertElementContainsText(likesCount + " Likes", "homePage.latestPostLikesCount");
        actions.unlikePost("homePage.latestPostLikeButton", unlikeText);
        likesCount--;
        actions.assertElementContainsText(likesCount + " Likes", "homePage.latestPostLikesCount");
    }

    @Test
    @Description("Validate that user is able to like and unlike other users' posts")
    public void OnPresentPostsOfOtherUsers_ClickOnPostLikeButton_LikeCountChanges() {
        int likesCount = 0;

        actions.logIn("userEmail", "userPassword");
        actions.createPublicPostWithText(postText);
        actions.logOut();
        actions.logIn("user3Email", "user3Password");
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.likeResource("homePage.latestPostLikeButton", unlikeText);
        likesCount++;
        actions.assertElementContainsText(likesCount + " Likes", "homePage.latestPostLikesCount");
        actions.unlikePost("homePage.latestPostLikeButton", unlikeText);
        likesCount--;
        actions.assertElementContainsText(likesCount + " Likes", "homePage.latestPostLikesCount");
    }

}