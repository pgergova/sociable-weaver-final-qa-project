package testCases;

import com.telerikacademy.uiframework.UserActions;
import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.*;

@Feature("Posts")
public class Test006_PostsNegativePaths extends BaseTest {
    private String message;
    private String postText = Utils.getConfigPropertyByKey("postValidText");
    private String alertForMissingInput = Utils.getConfigPropertyByKey("missingTextAlertMessage");
    private String tooLongPostText = Utils.getConfigPropertyByKey("tooLongText");
    private String croppedPostText = Utils.getConfigPropertyByKey("croppedText");

    @Test
    @Description("Validate that user is unable to create a post without text")
    public void OnBlankText_ClickShareButton_AlertMessageIsDisplayed() {
        actions.logIn("userEmail", "userPassword");

        int userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());

        actions.waitForElementVisible("homePage.newMessageTextarea", 800);
        actions.clickElement("homePage.newMessageShareButton");
        message = actions.findElement("homePage.newMessageTextarea").getAttribute("validationMessage");
        actions.assertStringsAreSame(alertForMissingInput, message);
        int currentUserPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        actions.assertNumbersAreEqual(userPostsCount, currentUserPostsCount);
    }

    @Test
    @Description("Validate that user is unable to create a post with too long text - 2001 characters")
    public void OnTooLongText_ClickOnShareButton_MessageGetsCroppedToTheLimit () {
        actions.logIn("userEmail", "userPassword");

        int userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());

        actions.createPrivatePostWithText(tooLongPostText);
        userPostsCount++;
        int currentUserPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        actions.assertElementContainsText(croppedPostText,"homePage.latestPostText");
        actions.assertNumbersAreEqual(userPostsCount, currentUserPostsCount);
    }

    @Test
    @Description("Validate that user is unable to updated a post with too long text - 2001 characters")
    public void OnTooLongText_ClickOnUpdatePostButton_MessageGetsCroppedToTheLimit() {
        actions.logIn("userEmail", "userPassword");
        actions.createPrivatePostWithText(postText);
        actions.waitForElementVisible("homePage.latestPostContainer", 800);
        actions.waitForElementClickable("homePage.latestPostUpdateButton", 800);
        actions.clickElement("homePage.latestPostUpdateButton");
        actions.updateField("homePage.latestPostUpdatePostTextarea", tooLongPostText, "homePage.latestPostUpdatePostButton");
        actions.assertElementContainsText(croppedPostText,"homePage.latestPostText");
    }

    @Test
    @Description("Validate that user is unable to update a post with blank text")
    public void OnBlankText_ClickOnUpdatePostButton_AlertMessageIsDisplayed() {
        actions.logIn("userEmail", "userPassword");

        int userPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());

        actions.createPublicPostWithText(postText);
        userPostsCount++;
        actions.waitForElementVisible("homePage.latestPostContainer", 800);
        actions.waitForElementClickable("homePage.latestPostUpdateButton", 800);
        actions.clickElement("homePage.latestPostUpdateButton");
        actions.updateField("homePage.latestPostUpdatePostTextarea", "", "homePage.latestPostUpdatePostButton");
        message = actions.findElement("homePage.latestPostUpdatePostTextarea").getAttribute("validationMessage");
        int currentUserPostsCount = Integer.parseInt(actions.findElement("homePage.userPostsCount").getText());
        actions.assertStringsAreSame(alertForMissingInput, message);
        actions.assertNumbersAreEqual(userPostsCount, currentUserPostsCount);
    }

    @Test
    @Description("Validate that user is unable to edit a post by another user")
    public void OnSearchOfPostByAnotherUser_NoPostUpdateButtonIsPresent() {
        actions.logIn("user3Email", "user3Password");
        actions.createPublicPostWithText(postText);
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.assertElementNotPresent("homePage.latestPostUpdateButton");
    }

    @Test
    @Description("Validate that user is unable to delete a post submitted by another user")
    public void OnSearchOfPostByAnotherUser_NoPostDeleteButtonIsPresent() {
        actions.logIn("user3Email", "user3Password");
        actions.createPublicPostWithText(postText);
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.assertElementNotPresent("homePage.latestPostDeleteButton");
    }

    @Test
    @Description("Validate that user is unable to change the visibility of a post by another user")
    public void OnSearchOfPostByAnotherUser_NoPrivacyButtonIsPresent() {
        actions.logIn("user3Email", "user3Password");
        actions.createPublicPostWithText(postText);
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.waitForElementVisible("homePage.latestPostContainer", 1000);
        actions.assertElementNotPresent("homePage.latestPostPrivacyButton");
    }
}
