package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.Test;

@Feature("Registration")
public class Test001_RegistrationNegativePaths extends BaseTestWithoutLogOut {
    private String validFirstName = Utils.getConfigPropertyByKey("validFirstName");
    private String validLastName = Utils.getConfigPropertyByKey("validLastName");
    private String validEmail = Utils.getConfigPropertyByKey("adminEmail");
    private String tooLongEmail = Utils.getConfigPropertyByKey("tooLongEmailAddress");
    private String invalidEmail = Utils.getConfigPropertyByKey("invalidEmail");
    private String alreadyRegisteredUserAlertMessage = Utils.getConfigPropertyByKey("alreadyRegisteredUserAlertMessage");

    @Test
    @Description("Validate that user is unable to register with blank first name")
    public void OnBlankFirstName_ClickRegisterButton_RegisterButtonIsDisabled() {
        actions.goToTheRegisterPage();
        actions.registerWithMissingInformation(validLastName, validEmail, "registerPage.lastNameField", "registerPage.emailField");
    }

    @Test
    @Description("Validate that user is unable to register with blank last name")
    public void OnBlankLastName_ClickRegisterButton_RegisterButtonIsDisabled() {
        actions.goToTheRegisterPage();
        actions.registerWithMissingInformation(validFirstName, validEmail, "registerPage.firstNameField", "registerPage.emailField");
    }

    @Test
    @Description("Validate that user is unable to register with blank email address")
    public void OnBlankEmail_ClickRegisterButton_RegisterButtonIsDisabled() {
        actions.goToTheRegisterPage();
        actions.registerWithMissingInformation(validFirstName, validLastName, "registerPage.firstNameField", "registerPage.lastNameField");
    }

    @Test
    @Description("Validate that user is unable to register with an already registered email")
    public void OnAlreadyRegisteredEmail_ClickRegisterButton_AlreadyRegisteredEmailAlert() {
        actions.goToTheRegisterPage();
        actions.updateFieldWithoutClickingOnButton("registerPage.firstNameField", validFirstName);
        actions.updateFieldWithoutClickingOnButton("registerPage.lastNameField", validLastName);
        actions.updateFieldWithoutClickingOnButton("registerPage.emailField", validEmail);
        actions.waitForElementClickable("registerPage.registerButton", 800);
        actions.clickElement("registerPage.registerButton");
        actions.waitForElementVisible("registerPage.alertContainer", 800);
        actions.assertElementContainsText(Utils.getConfigPropertyByKey("alreadyRegisteredUserAlertMessage"), "registerPage.alertContainer");
    }

    @Test
    @Description("Validate that user is unable to register with invalid email address")
    public void OnInvalidEmail_ClickOnRegisterButton_RegisterButtonIsDisabled() {
        actions.goToTheRegisterPage();
        actions.registerWithInvalidInformation(validFirstName, validLastName, invalidEmail);
    }

    @Test
    @Description("Validate that user is unable to register with too long email address")
    public void OnTooLongEmailAddress_ClickOnRegisterButton_RegisterButtonIsDisabled() {
        actions.goToTheRegisterPage();
        actions.registerWithInvalidInformation(validFirstName, validLastName, tooLongEmail);
    }

}
