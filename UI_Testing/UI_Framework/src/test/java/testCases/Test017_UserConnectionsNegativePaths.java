package testCases;

import io.qameta.allure.Description;
import org.junit.Test;

public class Test017_UserConnectionsNegativePaths extends BaseTestWithoutLogOut {
    @Test
    @Description("Validate that unregistered user is unable to request friendships")
    public void OnNotLoggedIn_SearchForRequestFriendshipLink_NoLinkIsPresent() {
        actions.goToLandingPage();
        actions.waitForElementVisible("landingPage.usersListContainer", 800);
        actions.assertElementDoesNotContainText("Request friendship", "landingPage.usersListContainer");
    }
}
