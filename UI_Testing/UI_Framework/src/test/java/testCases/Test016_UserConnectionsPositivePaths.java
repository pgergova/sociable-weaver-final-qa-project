package testCases;

import io.qameta.allure.Description;
import org.junit.Test;

public class Test016_UserConnectionsPositivePaths extends BaseTest {

    @Test
    @Description("Validate that user is able to submit and cancel a friendship request")
    public void OnPresentNotFriend_ClickOnRequestFriendshipLink_FriendshipRequestIsSent() {
        actions.logIn("userEmail", "userPassword");
        String currentUserNames = actions.findElement("homePage.findMoreBirdWatchersUser5").getText();
        actions.requestFriendship(currentUserNames, "homePage.findMoreBirdWatchersUser5", "profilePage.requestFriendshipLink");
        actions.clickElement("homePage.homePageLink");
        actions.waitForElementVisible("homePage.peopleYouRequestedFriendshipContainer", 800);
        actions.assertElementContainsText("Test User 5", "homePage.requestForUser5Container");
        actions.assertElementPresent("homePage.cancelFriendshipRequestForUser5");
        actions.clickElement("homePage.cancelFriendshipRequestForUser5");
        actions.assertElementNotPresent("homePage.cancelFriendshipRequestForUser5");
        actions.assertRequestFriendshipLinkIsNotPresent(currentUserNames, "homePage.findMoreBirdWatchersUser5", "profilePage.requestFriendshipLink");
    }

    @Test
    @Description("Validate that user is able to accept a friend request and disconnect from friend")
    public void OnPresentFriendRequest_ClickOnAcceptFriendRequest_UserBecomesFriend() {

        actions.logIn("userEmail", "userPassword");
        String currentUserNames = actions.findElement("homePage.findMoreBirdWatchersUser5").getText();
        actions.requestFriendship(currentUserNames, "homePage.findMoreBirdWatchersUser5", "profilePage.requestFriendshipLink");
        actions.clickElement("homePage.homePageLink");
        actions.logOut();
        actions.logIn("user5Email", "user5Password");
        int friendsCount = Integer.parseInt(actions.findElement("homePage.userFriendsCount").getText());
        int friendsCounter = friendsCount;
        actions.findElement("homePage.userFriendsCount").getText();
        actions.assertElementPresent("homePage.acceptFriendRequestLink");
        actions.clickElement("homePage.acceptFriendRequestLink");
        friendsCounter++;
        actions.assertElementNotPresent("homePage.requestFromUser1Container");
        friendsCount = Integer.parseInt(actions.findElement("homePage.userFriendsCount").getText());
        actions.assertNumbersAreEqual(friendsCounter, friendsCount);
        actions.waitForElementVisible("homePage.userFriendsList", 800);
        actions.waitForElementClickable("homePage.removeFromFriendlistLink", 800);
        actions.clickElement("homePage.removeFromFriendlistLink");
        friendsCounter--;
        friendsCount = Integer.parseInt(actions.findElement("homePage.userFriendsCount").getText());
        actions.assertNumbersAreEqual(friendsCounter, friendsCount);
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.assertRequestFriendshipLinkIsNotPresent(currentUserNames, "homePage.findMoreBirdWatchersUser5", "profilePage.requestFriendshipLink");
    }

    @Test
    @Description("Validate that user is able to decline a friendship request")
    public void OnPresentFriendshipRequest_ClickOnRejectLink_FriendshipRequestIsNoLongerPresent() {

        actions.logIn("userEmail", "userPassword");
        String currentUserNames = actions.findElement("homePage.findMoreBirdWatchersUser5").getText();
        actions.requestFriendship(currentUserNames, "homePage.findMoreBirdWatchersUser5", "profilePage.requestFriendshipLink");
        actions.logOut();
        actions.logIn("user5Email", "user5Password");
        int friendsCount = Integer.parseInt(actions.findElement("homePage.userFriendsCount").getText());
        int friendsCounter = friendsCount;
        actions.findElement("homePage.userFriendsCount").getText();
        actions.assertElementPresent("homePage.rejectFriendRequestLink");
        actions.clickElement("homePage.rejectFriendRequestLink");
        actions.assertElementNotPresent("homePage.requestFromUser1Container");
        friendsCount = Integer.parseInt(actions.findElement("homePage.userFriendsCount").getText());
        actions.assertNumbersAreEqual(friendsCounter, friendsCount);
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.assertRequestFriendshipLinkIsNotPresent(currentUserNames, "homePage.findMoreBirdWatchersUser5", "profilePage.requestFriendshipLink");
    }
}
