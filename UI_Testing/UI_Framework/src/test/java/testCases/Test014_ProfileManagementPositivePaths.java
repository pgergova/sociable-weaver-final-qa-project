package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.After;
import org.junit.Test;

@Feature("Profile Management")
public class Test014_ProfileManagementPositivePaths extends BaseTest {
    private String updatedFirstNameText = Utils.getConfigPropertyByKey("updatedValidFirstName");
    private String updatedLastNameText = Utils.getConfigPropertyByKey("updatedValidLastName");
    private String userNationality = Utils.getConfigPropertyByKey("userNationality");
    private String userCity = Utils.getConfigPropertyByKey("userCity");

    @Test
    @Description("Validate that user is able to change their first name")
    public void OnValidFirstName_ClickOnUpdateButton_FirstNameIsUpdated() {
        actions.goToProfilePage("userEmail", "userPassword");

        String userFirstName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserFirstName")).getText();

        actions.updateProfileFieldInformation("profilePage.firstNameInputField", updatedFirstNameText, "profilePage.submitFormButton");
        String currentFirstName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserFirstName")).getText();
        actions.assertStringsAreSame(currentFirstName, updatedFirstNameText);actions.waitForElementClickable("profilePage.updateProfileButton", 800);
        actions.updateProfileFieldInformation("profilePage.firstNameInputField", userFirstName, "profilePage.submitFormButton");
        currentFirstName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserFirstName")).getText();
        actions.assertStringsAreSame(userFirstName, currentFirstName);
    }

    @Test
    @Description("Validate that user is able to change their last name")
    public void OnValidLastName_ClickOnUpdateButton_LastNameIsUpdated() {
        actions.goToProfilePage("userEmail", "userPassword");

        String userLastName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserLastName")).getText();
        actions.updateProfileFieldInformation("profilePage.lastNameInputField", updatedLastNameText, "profilePage.submitFormButton");
        String currentLastName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserLastName")).getText();
        actions.assertStringsAreSame(currentLastName, updatedLastNameText);
        actions.updateProfileFieldInformation("profilePage.lastNameInputField", userLastName, "profilePage.submitFormButton");
        currentLastName = actions.findElement(Utils.getUIMappingByKey("profilePage.currentUserLastName")).getText();
        actions.assertStringsAreSame(userLastName, currentLastName);
    }

    @Test
    @Description("Validate that user is able to enter and remove nationality in their profile")
    public void OnValidNationality_ClickOnUpdateButton_NationalityIsSelected() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldInformation("profilePage.nationalityInputField", userNationality, "profilePage.submitFormButton");
        String currentNationality = actions.findElement("profilePage.currentUserNationality").getText();
        actions.assertStringsAreSame(currentNationality, userNationality);
        actions.updateProfileFieldInformation("profilePage.nationalityInputField", "", "profilePage.submitFormButton");
        actions.assertElementIsNotVisible("profilePage.currentUserNationality");
    }

    @Test
    @Description("Validate that user is able to enter and remove the city in their profile")
    public void OnValidCity_ClickOnUpdateButton_CityIsSelected() {
        actions.goToProfilePage("userEmail", "userPassword");
        actions.updateProfileFieldInformation("profilePage.cityInputField", userCity, "profilePage.submitFormButton");
        String currentNationality = actions.findElement("profilePage.currentUserCity").getText();
        actions.assertStringsAreSame(currentNationality, userCity);
        actions.updateProfileFieldInformation("profilePage.cityInputField", "", "profilePage.submitFormButton");
        actions.assertElementIsNotVisible("profilePage.currentUserCity");
    }
}
