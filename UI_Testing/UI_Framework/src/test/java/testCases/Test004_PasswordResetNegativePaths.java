package testCases;

import com.telerikacademy.uiframework.UserActions;
import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;

@Feature("LogIn")
public class Test004_PasswordResetNegativePaths extends BaseTestWithoutLogOut {
    private String incorrectEmail = Utils.getConfigPropertyByKey("incorrectEmail");
    private String invalidEmail = Utils.getConfigPropertyByKey("invalidEmail");
    private String disabledEmail = Utils.getConfigPropertyByKey("disabledEmailAddress");
    private String emptyEmailAlertMessage = Utils.getConfigPropertyByKey("forgotPasswordPageEmptyEmailAlertMessage");
    private String invalidEmailAlertMessage = Utils.getConfigPropertyByKey("forgotPasswordPageInvalidEmailAlertMessage");
    private String notFoundEmailAlertMessage = Utils.getConfigPropertyByKey("forgotPasswordPageNotFoundEmailAlertMessage");
    private String disabledEmailAlertMessage = Utils.getConfigPropertyByKey("forgotPasswordPageDisabledAccountAlertMessage");

    @Test
    @Description("Validate that user is unable to recover their password with blank email")
    public void OnEmptyEmailAddress_ClickOnResetPasswordButton_AlertIsReceived() {
        actions.resetPasswordWithInvalidData("", emptyEmailAlertMessage, "forgotPasswordPage.errorAlertMessage");
    }

    @Test
    @Description("Validate that user is unable to recover their password with invalid email")
    public void OnInvalidEmailAddress_ClickOnResetPasswordButton_AlertIsReceived() {
        actions.resetPasswordWithInvalidData(invalidEmail, invalidEmailAlertMessage, "forgotPasswordPage.errorAlertMessage");
    }

    @Test
    @Description("Validate that user is unable to recover password for unregistered email address")
    public void OnUnregisteredEmailAddress_ClickOnResetPasswordButton_AlertIsReceived() {
        actions.resetPasswordWithInvalidData(incorrectEmail, notFoundEmailAlertMessage, "forgotPasswordPage.errorAlertMessage");
    }

    @Test
    @Description("Validate that user is unable to recover password for disabled account")
    public void OnDisabledEmailAddress_ClickOnResetPasswordButton_AlertIsReceived() {
        actions.resetPasswordWithInvalidData(disabledEmail, disabledEmailAlertMessage, "forgotPasswordPage.errorAlertMessage");
    }
}
