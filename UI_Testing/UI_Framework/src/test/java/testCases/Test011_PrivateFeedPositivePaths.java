package testCases;

import com.telerikacademy.uiframework.Utils;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;

@Feature("Feeds")
public class Test011_PrivateFeedPositivePaths extends BaseTest {
    private String publicPostText = Utils.getConfigPropertyByKey("publicPostValidText");
    private String privatePostText = Utils.getConfigPropertyByKey("privatePostValidText");

    @Test
    @Description("Validate that user is able to see feed only with friends' posts")
    public void OnPresentPostsOfFriends_GoToHomePage_SeeFeedWithPostOfHisFriends() throws InterruptedException{
        actions.logIn("user3Email", "user3Password");
        actions.createPrivatePostWithText(privatePostText);
        actions.assertPostContainsText(privatePostText, "homePage.latestPostText");
        Thread.sleep(1000);
        actions.createPublicPostWithText(publicPostText);
        actions.assertPostContainsText(publicPostText, "homePage.latestPostText");
        Thread.sleep(1000);
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.waitForElementVisible("homePage.latestPostContainer", 800);
        actions.assertPostContainsText(publicPostText, "homePage.latestPostText");
        actions.assertPostContainsText(privatePostText, "homePage.secondPostInFeedContainer");
    }

    @Test
    @Description("Validate user is able to see public posts in non-friend's private profile feed")
    public void OnPresentPublicPostOfNonFriend_GoToUsersProfile_PublicPostIsVisible() {
        actions.logIn("user5Email", "user5Password");
        actions.createPublicPostWithText(publicPostText);
        actions.assertPostContainsText(publicPostText, "homePage.latestPostText");
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.goToPage("homePage.findMoreBirdWatchersUser5");
        String currentUserLastName = actions.findElement("profilePage.currentUserLastName").getText();
        actions.assertStringsAreSame("User 5", currentUserLastName);
        actions.assertElementContainsText(publicPostText, "profilePage.latestPostInFeedTextContainer");
    }


    @Test
    @Description("Validate user is able to see public post in non-friend's public profile feed")
    public void OnPresentPublicPostsOfNonFriend_GoToPublicProfileOfNonFriend_SeePublicPost() {
        actions.logIn("user6Email", "user6Password");
        actions.createPublicPostWithText(publicPostText);
        actions.assertPostContainsText(publicPostText, "homePage.latestPostText");
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.goToPage("homePage.findMoreBirdWatchersUser6");
        String currentUserLastName = actions.findElement("profilePage.currentUserLastName").getText();
        actions.assertStringsAreSame("User 6", currentUserLastName);
        if(actions.getSizeOfElements("profilePage.postContainer") > 1) {
            actions.assertElementContainsText(publicPostText, "profilePage.latestPostInFeedTextContainer");
        }
    }

    @Test
    @Description("Validate user is able to see private posts in friend's profile feed")
    public void OnPresentPrivatePostOfFriend_GoToFriendsProfilePage_PrivatePostIsVisible() {
        actions.logIn("user3Email", "user3Password");
        actions.createPrivatePostWithText(privatePostText);
        actions.assertPostContainsText(privatePostText, "homePage.latestPostText");
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.goToPage("homePage.userFriendOne");
        String currentUserLastName = actions.findElement("profilePage.currentUserLastName").getText();
        actions.assertStringsAreSame("User 3", currentUserLastName);
        actions.assertElementContainsText(privatePostText, "profilePage.latestPostInFeedTextContainer");
    }

    @Test
    @Description("Validate user is able to see public posts in friend's profile feed")
    public void OnPresentPublicPostOfFriend_GoToFriendsProfilePage_PublicPostIsVisible() {
        actions.logIn("user3Email", "user3Password");
        actions.createPrivatePostWithText(publicPostText);
        actions.assertPostContainsText(publicPostText, "homePage.latestPostText");
        actions.logOut();
        actions.logIn("userEmail", "userPassword");
        actions.goToPage("homePage.userFriendOne");
        String currentUserLastName = actions.findElement("profilePage.currentUserLastName").getText();
        actions.assertStringsAreSame("User 3", currentUserLastName);
        actions.assertElementContainsText(publicPostText, "profilePage.latestPostInFeedTextContainer");
    }

    @Test
    @Description("Validate that system admin is able to see private posts in non-friend's private profile feed")
    public void OnPrivatePostOfNonFriend_GoToProfilePage_SystemAdminIsAbleToSeePost() {
        actions.logIn("user5Email", "user5Password");
        actions.createPrivatePostWithText(privatePostText);
        actions.assertPostContainsText(privatePostText, "homePage.latestPostText");
        actions.logOut();
        actions.logIn("adminEmail", "adminPassword");
        actions.goToPage("homePage.findMoreBirdWatchersUser5");
        String currentUserLastName = actions.findElement("profilePage.currentUserLastName").getText();
        actions.assertStringsAreSame("User 5", currentUserLastName);
        actions.assertElementContainsText(privatePostText, "profilePage.latestPostInFeedTextContainer");
    }
}

